package test.stagecoach.Analysis;

/**
 * Created by colin on 11/20/2015.
 */
public class TimeRange implements Comparable{

    private long _msStart;
    private long _msEnd;

    public TimeRange(long msStart, long msEnd)
    {
        _msStart = msStart;
        _msEnd = msEnd;
    }

    public long getStart() {
        return _msStart;
    }

    public long getEnd() {
        return _msEnd;
    }

    public long roundStart(long chunkSize)
    {
        long remainder = _msStart%chunkSize;
        if(remainder < (chunkSize/2))
            return _msStart - remainder;
        else
            return _msStart * chunkSize - remainder;
    }

    public long roundEnd(long chunkSize)
    {
        long remainder = _msEnd%chunkSize;
        if(remainder < (chunkSize/2))
            return _msEnd - remainder;
        else
            return _msEnd * chunkSize - remainder;
    }

    public boolean contains (long ms)
    {
        return ms >= _msStart && ms <= _msEnd;
    }

    public boolean equals (TimeRange other)
    {
        return other.getStart() == _msStart && other.getEnd() == _msEnd;
    }

    @Override
    public int compareTo(Object o) {
        TimeRange other = (TimeRange)o;

        if(other.getStart() < _msStart)
            return -1;
        else if (other.getStart() == _msStart)
            return 0;
        else
            return 1;
    }
}
