package test.stagecoach.Analysis;

import android.util.Pair;

import com.androidplot.xy.XYPlot;

import java.util.ArrayList;
import java.util.List;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Utils.GraphType;

/**
 * Created by colin on 3/13/2016.
 */
public abstract class TranscriptPlugin extends Plugin {

    public TranscriptPlugin(String name)
    {
        super(name);
    }

    public abstract void run();

    @Override
    protected void finalize() {

    }

    @Override
    protected void processChunk(byte[] chunk) {
    }

    @Override
    public abstract Object getData();

    @Override
    public GraphType getType() {
        return GraphType.transcript;
    }

    @Override
    public abstract void ChangeGraphLook(XYPlot plot);

    @Override
    public boolean isTranscriptPlugin() {
        return true;
    }
}
