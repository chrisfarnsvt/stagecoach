package test.stagecoach.Analysis.Plugins;

import android.util.Pair;

import com.androidplot.xy.XYPlot;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.Feedback;
import test.stagecoach.Analysis.TimeRange;
import test.stagecoach.Analysis.TranscriptPlugin;
import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;

/**
 * Created by chris_000 on 3/15/2016.
 */
public class WordOverusePlugin extends TranscriptPlugin {

    private List<Pair<String, Integer>> _wordCounts; //for documentation

    public WordOverusePlugin() {
        super ("wordOveruse");
        _wordCounts = new ArrayList<>();
    }

    @Override
    public void run() {
            List<String> words = STTManager.getInstance().getWords();
            for (String word : words) {
                boolean set = false;
                List<Pair<String, Integer>> temp = new ArrayList<>();
                temp.addAll(_wordCounts);
                for (Pair<String, Integer> count : _wordCounts) {
                    String thisWord = count.first;
                    int counter = count.second;
                    if (thisWord.equals(word)) {
                        temp.remove(count);
                        temp.add(new Pair<String, Integer>(thisWord, counter + 1));
                        set = true;
                    }
                }
                if (!set) {
                    temp.add(new Pair<String, Integer>(word, 1));
                }
                _wordCounts = temp;
            }

            int i = 0, j = 0;
            //for (String word : words) {
                for (Pair<String, Integer> count : _wordCounts) {
                    Map<TimeRange, List<Feedback>> temp = new HashMap<>();
                    temp.putAll(_feedback);
                    String thisWord = count.first;
                    int counter = count.second;
                   // if (thisWord.equals(word)) {
                        //TimeRange tr = new TimeRange(i, j);
                       // if (timeRangeExists(tr)) {
                        //    temp.get(tr).add(new WordOveruseFeedback(thisWord, counter));
                       // }
                        //else {
                            List<Feedback> newFeed = new ArrayList<>();
                            newFeed.add(new WordOveruseFeedback(thisWord, counter));
                            temp.put(new TimeRange(i, j), newFeed);
                        //}
                        i++;
                        j++;
                   // }
                    _feedback = temp;
                }
            //}

        int maxcount = 0;
        String mostUsed = "";
        for(Pair<String, Integer> p : _wordCounts)
        {
            if(p.second > maxcount)
            {
                maxcount = p.second;
                mostUsed = p.first;
            }
        }

        if (mostUsed.equals(""))
            m_quickFeedback = "Your speech had no words, try speaking.";
        else
            m_quickFeedback = "Your most commonly used word was " + mostUsed + ". You used it " + maxcount + " times.";
    }

    @Override
    public Object getData() {
        return _wordCounts;
    }

    @Override
    public void ChangeGraphLook(XYPlot plot) {

    }

    public GraphType getType() {
        return GraphType.none;
    }

    @Override
    public String getXMLTag() {
        String result = "";
        result += "\t<WordOveruse>\n";
        for (Pair<String, Integer> count : _wordCounts) {
            result += "\t\t<WordCount>\n";

            String s = count.first;
            int num = count.second;

            result += "\t\t\t<Word>" + s + "</Word>\n";
            result += "\t\t\t<Count>" + num + "</Count>\n";

            result += "\t\t</WordCount>\n";
        }
        result += "\t\t<Score>" + Integer.toString(percentScore) + "</Score>\n";
        result += "\t\t<quickFeedback>" + m_quickFeedback + "</quickFeedback>\n";
        result += "\t</WordOveruse>\n";

        return result;
    }

    @Override
    public void readXMLTag() {

        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();
        Element overuseElem = (Element)(doc.getElementsByTagName("WordOveruse").item(0));

        NodeList nList = overuseElem.getElementsByTagName("WordCount");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                String s = elem.getElementsByTagName("Word").item(0).getTextContent();
                int count = Integer.parseInt(elem.getElementsByTagName("Count").item(0).getTextContent());
                _wordCounts.add(new Pair(s, count));
                TimeRange tr = new TimeRange(i, i);

                _feedback.put(tr, new ArrayList<Feedback>());
                _feedback.get(tr).add(new WordOveruseFeedback(s, count));
            }
        }
        percentScore = Integer.parseInt(overuseElem.getElementsByTagName("Score").item(0).getTextContent());
        m_quickFeedback = overuseElem.getElementsByTagName("quickFeedback").item(0).getTextContent();
    }
}
