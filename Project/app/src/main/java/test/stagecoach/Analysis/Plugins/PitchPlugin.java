package test.stagecoach.Analysis.Plugins;

import android.graphics.Color;
import android.util.Pair;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.Complex;
import test.stagecoach.Analysis.FFT;
import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.SCPlotManager;
import test.stagecoach.Utils.GraphType;

/**
 * Created by colin on 2/27/2016.
 */
public class PitchPlugin extends Plugin{
    private int m_sampleRate;
    private List<Pair<Number, Number>> m_graphData;
    private int m_frequencySum = 0;
    private int m_frequencyCount = 0;

   public PitchPlugin(PluginManager manager, int sampleRate) {
       super("pitch");
       m_sampleRate = sampleRate;
       m_graphData = new ArrayList<Pair<Number, Number>>();

       test();
   }

    private void test()
    {
        float FR = 8192f/16000f;
        Complex[] buffer = new Complex[8192];
        for(int i = 0; i < 8192; i++)
        {
            buffer[i] = new Complex(Math.sin(1000 * (2 * Math.PI) * i / 16000), 0);
        }
        Complex[] out = FFT.fft(buffer);

        double max = Long.MIN_VALUE;
        float frequency = 0;
        for (int i = 0; i < out.length/2; i++) {
            Complex c = out[i];
            float magnitude = (float)Math.sqrt( (c.re()*c.re()+c.im()*c.im()));
            if (magnitude > max) {
                max = magnitude;
                frequency = i;
            }
        }

        frequency = frequency/FR;

        int test = 0;
    }


   @Override
   protected void processChunk(byte[] chunk) {
       //agreed
       Complex[] in = new Complex[4096];
       Complex[] out;
       int pos = 0;
       for (int i = 0; i < 8192; i+=2) {
           short sample = (short) ((chunk[i + 1] << 8) | chunk[i]);
           in[pos] = new Complex((double) sample, 0);
           pos++;
       }
       for(int i = 0; i < 4096; i++)
       {
          if(in[i] == null)
              in[i] = new Complex(0,0);
       }
       out = FFT.fft(in);

       double max = Long.MIN_VALUE;
       float frequency = 0;
       for (int i = 0; i < out.length/2; i++) {
           Complex c = out[i];
           float magnitude = (float)Math.sqrt( (c.re()*c.re()+c.im()*c.im()));
           if (magnitude > max) {
               max = magnitude;
               frequency = (float)i;
           }
       }
       float FR = 8192f/16000f;
       frequency = frequency/FR;
       if(frequency > 400)
       {
        //outlier, put 0 instead
           float second = _manager.getChunkNum();
           m_graphData.add(new Pair<Number, Number>(second, frequency));
       }
       else {
           float second = _manager.getChunkNum();
           m_graphData.add(new Pair<Number, Number>(second, frequency));
           m_frequencySum += frequency;
           m_frequencyCount++;
       }
   }

   @Override
   protected void finalize()
   {
       float averageFreq = m_frequencySum/ m_frequencyCount;
        m_quickFeedback = "Your average pitch was " + averageFreq + "Hz. You varied your pitch well.";
   }

   @Override
   public Object getData() {
       return m_graphData;
   }

   @Override
   public GraphType getType() {
       return GraphType.xy_scatter;
   }

   @Override
   public void ChangeGraphLook(XYPlot plot) {

       plot.getTitleWidget().setText("Pitch vs Time");
       plot.getDomainLabelWidget().setText("Frequency (Hz)");
       plot.setRangeBoundaries(0, 400, BoundaryMode.FIXED);
       ArrayList<Number> xVals = new ArrayList<>();
       ArrayList<Number> yVals = new ArrayList<>();
       for(Pair<Number, Number> pair : m_graphData)
       {
           xVals.add(pair.first);
           yVals.add(pair.second);
       }
       // Turn the above arrays into XYSeries':
       XYSeries series1 = new SimpleXYSeries(
               xVals,
               yVals,
               "Pitch Data");

       SCPlotManager.getInstance().addSeries(series1, Color.rgb(0, 255, 0));
   }

    @Override
    public String getXMLTag() {
        String result = "";
        result += "\t<Pitch>\n";
        for (Pair<Number, Number> dataPoint : m_graphData) {
            result += "\t\t<PitchData>\n";

            float x = dataPoint.first.floatValue();
            float y = dataPoint.second.floatValue();

            result += "\t\t\t<x>" + Float.toString(x) + "</x>\n";
            result += "\t\t\t<y>" + Float.toString(y) + "</y>\n";

            result += "\t\t</PitchData>\n";
        }
        result += "\t\t<score>" + Integer.toString(percentScore) + "</score>\n";
        result += "\t\t<quickFeedback>" + m_quickFeedback + "</quickFeedback>\n";
        result += "\t</Pitch>\n";
        return result;
    }

    @Override
    public void readXMLTag() {

        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();

        Element pitch = (Element)(doc.getElementsByTagName("Pitch").item(0));
        NodeList nList = pitch.getElementsByTagName("PitchData");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                float x = Float.parseFloat(elem.getElementsByTagName("x").item(0).getTextContent());
                float y = Float.parseFloat(elem.getElementsByTagName("y").item(0).getTextContent());
                m_graphData.add(new Pair(x,y));
            }
        }
        percentScore = Integer.parseInt(pitch.getElementsByTagName("score").item(0).getTextContent());
        m_quickFeedback = pitch.getElementsByTagName("quickFeedback").item(0).getTextContent();
    }

}
