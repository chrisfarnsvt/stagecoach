package test.stagecoach.Analysis.Plugins;

import test.stagecoach.Analysis.Feedback;

/**
 * Created by chris_000 on 11/29/2015.
 */
public class VolumeFeedback implements Feedback {

    private double m_db;

    public VolumeFeedback(double db) {
        m_db = db;
    }

    @Override
    public String getInfo() {
        return  m_db + " decibels";
    }

    public double getVolume() {
        return m_db;
    }

    @Override
    public int getDeduction() {
        return 5;
    }
}
