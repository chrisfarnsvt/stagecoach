package test.stagecoach.Analysis;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.stagecoach.Analysis.Plugins.PausesFeedback;
import test.stagecoach.Analysis.Plugins.PausesPlugin;
import test.stagecoach.Analysis.Plugins.PitchPlugin;
import test.stagecoach.Analysis.Plugins.RatePlugin;
import test.stagecoach.Analysis.Plugins.SeparatorPlugin;
import test.stagecoach.Analysis.Plugins.VolumePlugin;
import test.stagecoach.Analysis.Plugins.WordOverusePlugin;
import test.stagecoach.Utils.SCAudioManager;

/**
 * Created by colin on 11/6/2015.
 */
public class PluginManager {

    private static PluginManager m_instance; //singleton

    private ArrayList<Plugin> m_loadedPlugins;
    private List<TranscriptPlugin> m_transcriptPlugins;
    private Map<TimeRange, Feedback> m_combinedFeedback;
    private BufferedInputStream m_buf;

    private int CHUNK_SIZE;
    private byte[] m_currentChunk;

    public static PluginManager getInstance()
    {
        if(m_instance == null)
            m_instance = new PluginManager();

        return m_instance;
    }

    private PluginManager()
    {
        m_loadedPlugins = new ArrayList<>();
        m_transcriptPlugins = new ArrayList<>();
        m_combinedFeedback = new HashMap<>();
    }

    public List<Plugin> getPlugins()
    {
        List<Plugin> allPlugins = new ArrayList<>();
        allPlugins.addAll(m_loadedPlugins);
        allPlugins.addAll(m_transcriptPlugins);

        return allPlugins;
    }

    public List<Plugin> getAudioPlugins()
    {
        return m_loadedPlugins;
    }

    public List<TranscriptPlugin> getTranscriptPlugins()
    {
        return m_transcriptPlugins;
    }

    public void gatherFeedback()
    {
        //combine all feedback maps from plugins
        for(Plugin p : m_loadedPlugins)
        {
            //m_combinedFeedback.putAll(p.getFeedback());
        }
    }

    public Map<TimeRange, Feedback> getCombinedFeedback()
    {
        return m_combinedFeedback;
    }

    public void reset()
    {
        m_loadedPlugins.clear();
        m_transcriptPlugins.clear();
        m_currentChunk = null;
        m_buf = null;
        m_chunkNum = 0;
        m_combinedFeedback.clear();
    }
    public boolean addPlugin(Plugin p) {
        boolean success = true;
        try {
            checkIfCanAdd(p);
            m_loadedPlugins.add(p);
        } catch(BadPluginException bad) {
            Log.d("DEBUG", "We loaded a duplicate plugin somehow oh dang");
            bad.printStackTrace();
            success = false;
        }

        return success;
    }

    private int m_chunkNum = 0;

    public int getChunkNum()
    {
        return m_chunkNum - 1;
    }

    public void loadNextChunk()
    {
        try {
            if (m_buf.available() == 0) {
                m_currentChunk = null;
                return; // throw exception?
            }

            m_buf.read(m_currentChunk, 0, CHUNK_SIZE);
            m_chunkNum++;
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public byte[] getChunk()
    {
        return m_currentChunk;
    }


    /**
     * If we find a duplicate plugin id, throw a most righteous exception
     * @param newPlugin The plugin to test
     * @throws BadPluginException Thrown if the plugin we're testing matches an existing one
     */
    private void checkIfCanAdd(Plugin newPlugin) throws BadPluginException {
        for (Plugin p : m_loadedPlugins) {
            if (p.equals(newPlugin))
                throw new BadPluginException();
        }
    }

    /**
     * Gets plugin whose ID is "s". Useful for traversing dependencies
     * @param s
     * @return
     */
    public Plugin pluginFromString(String s) {
        for (Plugin pl : m_loadedPlugins) {
            if (pl.getID().equals(s))
                return pl;
        }
        return null;
    }

    public void loadDependencies(Plugin p) {
        for (String ID: p.getDependencies()){
            loadPlugin(ID);
        }
    }

    public boolean loadPlugin(String ID) {

        if(ID.equals("pitch"))
        {
            PitchPlugin pp = new PitchPlugin(this, SCAudioManager.getInstance().getRecorderSamplerate());
            addPlugin(pp);
            return true;
        }
        if(ID.equals("volume"))
        {
            VolumePlugin vp = new VolumePlugin(this, SCAudioManager.getInstance().getRecorderSamplerate());
            addPlugin(vp);
            return true;
        }

        if(ID.equals("pauses"))
        {
            for(Plugin p : m_loadedPlugins) {
                if(p.getID() == "volume") {
                    PausesPlugin pp = new PausesPlugin(this, SCAudioManager.getInstance().getRecorderSamplerate(), (VolumePlugin)p);
                    addPlugin(pp);
                    return true;
                }

                return false;
            }
        }

        if(ID == "rate") {
            RatePlugin rp = new RatePlugin();
            m_transcriptPlugins.add(rp);
            return true;
        }

        if (ID.equals("wordOveruse")) {
            WordOverusePlugin wop = new WordOverusePlugin();
            m_transcriptPlugins.add(wop);
            return true;
        }

        if(ID.equals("separators")) {
            SeparatorPlugin sp = new SeparatorPlugin();
            m_transcriptPlugins.add(sp);
            return true;
        }

        return false;
    }

    public void loadAllPlugins()
    {
        //utility method for loading everything
        loadPlugin("volume");
        loadPlugin("pauses");
        loadPlugin("pitch");
        loadPlugin("rate");
        loadPlugin("wordOveruse");
        loadPlugin("separators");
    }

    List<Thread> pluginThreads;
    List<Plugin> activePlugins;
    public void process()
    {
        //process 1 second at a time
        CHUNK_SIZE = SCAudioManager.getInstance().getRecorderSamplerate();

        File audio = new File(SCAudioManager.getInstance().getRecordingLocation());

        pluginThreads = new ArrayList<>();
        activePlugins = new ArrayList<>();

        int finished = 0;
        while(finished != m_loadedPlugins.size()) {
            m_chunkNum = 0;
            try {
                m_buf = new BufferedInputStream(new FileInputStream(audio));
            } catch(FileNotFoundException e)
            {
                e.printStackTrace();
            }

            m_currentChunk = new byte[CHUNK_SIZE];

            for (Plugin p : m_loadedPlugins) {
                if (p.isFinished())
                    continue;

                boolean allDependenciesLoaded = true;
                for (String dep : p.getDependencies()) {
                    if (!getPlugin(dep).isFinished())
                        allDependenciesLoaded = false;
                }
                if (!allDependenciesLoaded)
                    continue;

                pluginThreads.add(new Thread(p));
                activePlugins.add(p);
            }

            while(true) {
                loadNextChunk();
                if(m_currentChunk == null)
                    break;

                for (Thread t : pluginThreads) {
                    t.start();
                }

                try {
                    for (Thread t : pluginThreads) {
                        t.join();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pluginThreads.clear();
                for(Plugin p : activePlugins)
                {
                    pluginThreads.add(new Thread(p));
                }
            }
            for(Plugin p : activePlugins)
            {
                p.finalize();
                p.setFinished(true);
                finished++;
            }
            pluginThreads.clear();
            activePlugins.clear();
        }

        for (TranscriptPlugin t : m_transcriptPlugins)
        {
            pluginThreads.add(new Thread (t));
        }

        for(Thread t : pluginThreads)
        {
            t.run();
        }

        for(Thread t : pluginThreads)
        {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    //Do something with this for realz, for now it's just an exception really..
    private static class BadPluginException extends Exception {

    }

    public Plugin getPlugin(String id)
    {
        for(Plugin p : m_loadedPlugins)
        {
            if(p.getID().equals(id))
                return p;
        }

        for (TranscriptPlugin tp : m_transcriptPlugins) {
            if (tp.getID().equals(id))
                return tp;
        }

        return null;
    }

    public int getAverageScore()
    {
        int sum = 0;
        int count = m_loadedPlugins.size() + m_transcriptPlugins.size();
        for (Plugin p : getPlugins())
        {
            sum += p.getPercentScore();
        }

        return sum/count;
    }
}
