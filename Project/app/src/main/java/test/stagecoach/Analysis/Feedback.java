package test.stagecoach.Analysis;

/**
 * Created by colin on 11/20/2015.
 */
public interface Feedback {

    public abstract String getInfo();
    public abstract int getDeduction();
}
