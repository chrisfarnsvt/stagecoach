package test.stagecoach.Analysis.Plugins;

import android.util.Pair;

import com.androidplot.xy.XYPlot;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.Feedback;
import test.stagecoach.Analysis.TimeRange;
import test.stagecoach.Analysis.TranscriptPlugin;
import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;

/**
 * Created by colin on 4/1/2016.
 */
public class SeparatorPlugin extends TranscriptPlugin {

    int m_separatorCount = 0;
    public SeparatorPlugin()
    {
        super ("separators");
    }

    @Override
    public void run() {
        List<String> words = STTManager.getInstance().getWords();
        Map<Integer, Pair<Integer, Integer>> segs = STTManager.getInstance().getSegments();

        for(int i = 0;  i< words.size(); i++) {
            String w = words.get(i);
            if (w.equals("[UM]") ||
                    w.equals("[AH]") ||
                    w.equals("[AH]") ||
                    w.equals("[M]") ||
                    w.equals("[UH]") ||
                    w.equals("[UH]") ||
                    w.equals("[UM]") ||
                    w.equals("[UM]") ||
                    w.equals("[UM]") ||
                    w.equals("[ER]")) {

                m_separatorCount++;
                percentScore -= 5;
                List feedbacks = _feedback.get(new TimeRange(i, i));
                if(feedbacks == null) {
                    List<Feedback> newFeed = new ArrayList<>();
                    newFeed.add(new SeparatorFeedback(i));
                    _feedback.put(new TimeRange(i, i), newFeed);
                }
            }
        }

        if(m_separatorCount == 0)
            m_quickFeedback = "You didnt use any separator words while speaking. Nice Job!";
        else
            m_quickFeedback = "You used " + m_separatorCount + " separator words in your speech.";
    }

    @Override
    public Object getData() {
        return null;
    }

    @Override
    public void ChangeGraphLook(XYPlot plot) {

    }

    public GraphType getType() {
        return GraphType.none;
    }

    @Override
    public String getXMLTag() {

        String result = "";
        result += "\t<Separators>\n";
        for (Map.Entry<TimeRange, List<Feedback>> entry : _feedback.entrySet()) {

            SeparatorFeedback feedback = (SeparatorFeedback)entry.getValue().get(0);

            result += "\t\t<Separator>\n";

            result += "\t\t\t<Word>" + feedback.getWordNum() + "</Word>\n";

            result += "\t\t</Separator>\n";
        }
        result += "\t\t<Score>" + Integer.toString(percentScore) + "</Score>\n";
        result += "\t\t<quickFeedback>" + m_quickFeedback + "</quickFeedback>\n";
        result += "\t</Separators>\n";

        return result;
    }

    @Override
    public void readXMLTag() {
        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();
        Element overuseElem = (Element)(doc.getElementsByTagName("Separators").item(0));

        NodeList nList = overuseElem.getElementsByTagName("Separator");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                String s = elem.getElementsByTagName("Word").item(0).getTextContent();
                int count = Integer.parseInt(elem.getElementsByTagName("Count").item(0).getTextContent());
                List feedbacks = _feedback.get(new TimeRange(count, count));
                if(feedbacks == null)
                    feedbacks = _feedback.put(new TimeRange(count, count), new ArrayList<Feedback>());

                feedbacks.add(new SeparatorFeedback(count));
            }
        }
        percentScore = Integer.parseInt(overuseElem.getElementsByTagName("Score").item(0).getTextContent());
        m_quickFeedback = overuseElem.getElementsByTagName("quickFeedback").item(0).getTextContent();
    }
}
