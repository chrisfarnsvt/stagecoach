package test.stagecoach.Analysis.Plugins;

import test.stagecoach.Analysis.Feedback;

/**
 * Created by colin on 1/31/2016.
 */
public class PausesFeedback  implements Feedback {

    public PausesFeedback()
    {
    }

    @Override
    public String getInfo() {
        return "pause here";
    }

    @Override
    public int getDeduction() {
        return 1;
    }
}
