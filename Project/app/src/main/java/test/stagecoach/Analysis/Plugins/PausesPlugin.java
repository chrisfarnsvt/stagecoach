package test.stagecoach.Analysis.Plugins;

import android.graphics.Color;
import android.util.Pair;

import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.SCPlotManager;
import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.SCAudioManager;

/**
 * Created by colin on 1/31/2016.
 */
public class PausesPlugin extends Plugin {

    public PausesPlugin(PluginManager manager, int sampleRate, VolumePlugin vp) {
        super("pauses");
        m_sampleRate = sampleRate;
        m_graphData = new ArrayList<>();
        m_vp = vp;
        _dependencyIDs.add("volume");
    }

    private int m_sampleRate;
    private List<Pair<List<Pair<Number, Number>>, Boolean>> m_graphData; //a list of graph coordinates and a boolean which indicates it is
                                                                         //pause data (true) or volume data(false)
    private VolumePlugin m_vp;
    private boolean m_complete = false;

    @Override
    protected void processChunk(byte[] chunk) {
        if(!m_complete)
        {
            ArrayList<Pair<Number, Number>> volumeData = (ArrayList<Pair<Number, Number>>)(m_vp.getData());
            //ArrayList<Pair<Number, Number>> data = new ArrayList<Pair<Number, Number>>();

            int minVolume = Integer.MAX_VALUE;
            int maxVolume = Integer.MIN_VALUE;
            for(Pair<Number, Number> volume: volumeData)
            {
                if( volume.second.intValue() != 0 && volume.second.intValue() < minVolume)
                    minVolume = volume.second.intValue();

                if(volume.second.intValue() > maxVolume)
                    maxVolume = volume.second.intValue();
            }

            int pauseThreshold = minVolume + 5;
            //int pauseThreshold = 40;
            
            //if we are too close to the max we cant detect pauses, there is not enough variation in their speech
            if(maxVolume - pauseThreshold < 10)
            {
                pauseThreshold = 0;
            }

            List<Pair<Number, Number>> currentPause = new ArrayList();
            List<Pair<Number, Number>> currentVolume = new ArrayList();
            boolean inPauseRun = false;
            int runCount = 0; // how many sections (1/4 s) long is this pause
            for(Pair<Number, Number> p : volumeData)
            {
                if(p.second.intValue() < pauseThreshold) {
                    if(!inPauseRun)
                    {
                        //our Volume run is over, add to master list
                        currentPause = new ArrayList<>();
                        //currentPause.add(currentVolume.get(currentVolume.size()-1)); // add 1 extra data point to connect the lines
                        currentVolume.add(p);
                        m_graphData.add(new Pair<List<Pair<Number, Number>>, Boolean>(currentVolume, false));
                        currentVolume = new ArrayList<>();
                        inPauseRun = true;
                    }
                    currentPause.add(p);
                    runCount++;
                }
                else
                {
                    //our pause run is over
                    if(inPauseRun)
                    {
                        //if our pause was longer than a second and a half add it to the master list
                        if(runCount > 2) {
                            currentVolume.add(currentPause.get(currentPause.size()-1));
                            m_graphData.add(new Pair<List<Pair<Number, Number>>, Boolean>(currentPause, true));
                            runCount = 0;
                        }
                        else
                        {
                            //what we thought was a pause was just volume
                            for(Pair<Number, Number> pair : currentPause)
                            {
                                currentVolume.add(pair);
                            }
                            currentPause = new ArrayList<>();
                            runCount = 0;
                        }
                        inPauseRun = false;
                    }
                    currentVolume.add(p);
                }

            }
            if(currentPause.size() > 0)
                m_graphData.add(new Pair<List<Pair<Number, Number>>, Boolean>(currentPause, true));
            else if (currentVolume.size() > 0)
                m_graphData.add(new Pair<List<Pair<Number, Number>>, Boolean>(currentVolume, false));
            m_complete = true;
        }
    }

    @Override
    protected void finalize() {
        //every 30s of the speech should have at least one pause
        int ideal = (SCAudioManager.getInstance().getDurationMS()/1000)/30;

        int pauseCount = 0;
        for(Pair<List<Pair<Number, Number>>, Boolean> p : m_graphData)
        {
            if(p.second)
                pauseCount++;
        }

        if(pauseCount < ideal) {
            percentScore -= (ideal - pauseCount) * 10;
            m_quickFeedback = "You paused " + pauseCount + " times in this speech. We recommend " + ideal + " pauses for a speech of this length.";
        }
        else
        {
            m_quickFeedback = "You paused " + pauseCount + " times in this speech. This is about right for a speech of this length.";
        }

    }

    @Override
    public Object getData() {
        return m_graphData;
    }

    @Override
    public GraphType getType() {
        return GraphType.xy_scatter;
    }

    @Override
    public void ChangeGraphLook(XYPlot plot) {
        SCPlotManager pm = SCPlotManager.getInstance();

        for(Pair<List<Pair<Number, Number>>, Boolean> pair : m_graphData)
        {
            //this is a pause series
            if(pair.second)
            {
                ArrayList<Number> xVals = new ArrayList<>();
                ArrayList<Number> yVals = new ArrayList<>();
                for(Pair<Number, Number> p : pair.first)
                {
                    xVals.add(p.first);
                    yVals.add(p.second);
                }

                XYSeries pauseSeries = new SimpleXYSeries(xVals, yVals, "Pause");
                pm.addSeries(pauseSeries, Color.rgb(255,0,0));
            }
            //this is a volume series
            else
            {
                ArrayList<Number> xVals = new ArrayList<>();
                ArrayList<Number> yVals = new ArrayList<>();
                for(Pair<Number, Number> p : pair.first)
                {
                    xVals.add(p.first);
                    yVals.add(p.second);
                }

                XYSeries pauseSeries = new SimpleXYSeries(xVals, yVals, "volume");
                pm.addSeries(pauseSeries, Color.rgb(0,255,0));
            }
        }
    }

    @Override
    public String getXMLTag() {
        String result = "";
        result += "\t<Pauses>\n";
        for (Pair<List<Pair<Number, Number>>, Boolean> series : m_graphData) {
            if(series.second)
                result += "\t\t<PausesSeries isPauseSeries=\"true\">\n";
            else
                result += "\t\t<PausesSeries isPauseSeries=\"false\">\n";
            for(Pair<Number, Number> dataPoint : series.first) {
                result += "\t\t\t<PausesData>\n";

                float x = dataPoint.first.floatValue();
                float y = dataPoint.second.floatValue();

                result += "\t\t\t\t<x>" + Float.toString(x) + "</x>\n";
                result += "\t\t\t\t<y>" + Float.toString(y) + "</y>\n";

                result += "\t\t\t</PausesData>\n";
            }
            result += "\t\t</PausesSeries>\n";
        }
        result += "\t\t<score>" + Integer.toString(percentScore) + "</score>\n";
        result += "\t\t<quickFeedback>" + m_quickFeedback + "</quickFeedback>\n";
        result += "\t</Pauses>\n";

        return result;
    }

    @Override
    public void readXMLTag() {

        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("Pauses");
        Node pauseNode = nList.item(0);
        Element pauseElem = (Element) pauseNode;
        NodeList series = pauseElem.getElementsByTagName("PausesSeries");
        for (int i = 0; i < series.getLength(); i++) {
            Node seriesNode = series.item(i);
            boolean isPauseData = seriesNode.getAttributes().getNamedItem("isPauseSeries").getNodeValue().equals("true");
            List<Pair<Number, Number>> seriesData = new ArrayList<>();
            if(seriesNode.getNodeType() == Node.ELEMENT_NODE) {
                Element seriesElem = (Element) seriesNode;
                NodeList data = seriesElem.getElementsByTagName("PausesData");
                for(int j = 0; j < data.getLength(); j++) {
                    Node dataNode = data.item(j);
                    Element dataElem = (Element) dataNode;
                    float x = Float.parseFloat(dataElem.getElementsByTagName("x").item(0).getTextContent());
                    float y = Float.parseFloat(dataElem.getElementsByTagName("y").item(0).getTextContent());

                    seriesData.add(new Pair(x, y));
                }
            }

            m_graphData.add(new Pair<>(seriesData, isPauseData));
        }

        percentScore = Integer.parseInt(pauseElem.getElementsByTagName("score").item(0).getTextContent());
        m_quickFeedback = pauseElem.getElementsByTagName("quickFeedback").item(0).getTextContent();
    }

}
