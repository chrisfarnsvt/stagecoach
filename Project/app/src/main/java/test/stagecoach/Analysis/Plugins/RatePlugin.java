package test.stagecoach.Analysis.Plugins;

import android.util.Pair;

import com.androidplot.xy.XYPlot;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.TranscriptPlugin;
import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;

/**
 * Created by colin on 3/13/2016.
 *
 * 80 - 100 words per minute is perfect
 */
public class RatePlugin extends TranscriptPlugin {

    public RatePlugin()
    {
        super("rate");

    }

    private float m_wpm = 0;
    @Override
    public void run() {
        int numWords = STTManager.getInstance().getWords().size();
        if(numWords == 0)
        {
            percentScore = 0;
            m_quickFeedback = "I didnt hear any words.";
            return;
        }
        int seconds = SCAudioManager.getInstance().getDurationMS()/1000;

        m_wpm = (float)numWords/(float)seconds*60f;
        if(m_wpm < 80)
        {
            percentScore = 100 - (80 - (int)m_wpm);
            m_quickFeedback = "You spoke at " + m_wpm + " words per minute. Try speaking a little more quickly.";
        }
        if (m_wpm > 100)
        {
            percentScore = 100 - ((int)m_wpm - 100);
            m_quickFeedback = "You spoke at " + m_wpm + " words per minute. Try slowing your speech down a little.";
        }

    }

    @Override
    public Object getData() {
        return null;
    }

    @Override
    public void ChangeGraphLook(XYPlot plot) {

    }

    public GraphType getType() {
        return GraphType.none;
    }

    @Override
    public String getXMLTag() {
        String result = "";
        result += "\t<RateData>\n";
        result += "\t\t<wpm>" + Float.toString(m_wpm) + "</wpm>\n";
        result += "\t\t<score>" + Integer.toString(percentScore) + "</score>\n";
        result += "\t\t<quickFeedback>" + m_quickFeedback + "</quickFeedback>\n";
        result += "\t</RateData>\n";
        return result;
    }

    @Override
    public void readXMLTag() {

        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("RateData");
        Element elem = (Element)nList.item(0);

        m_wpm = Float.parseFloat(elem.getElementsByTagName("wpm").item(0).getTextContent());
        percentScore = Integer.parseInt(elem.getElementsByTagName("score").item(0).getTextContent());
        m_quickFeedback = elem.getElementsByTagName("quickFeedback").item(0).getTextContent();
    }
}
