package test.stagecoach.Analysis.Plugins;

import android.graphics.Color;
import android.util.Pair;

import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.Feedback;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Analysis.TimeRange;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.SCPlotManager;
import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.XMLable;

/**
 * Created by chris_000 on 11/28/2015.
 */
public class VolumePlugin extends Plugin implements XMLable {

    private int m_sampleRate;
    private List<Pair<Number, Number>> m_graphData;
    private double m_volumeSum;
    private int m_volumeCount;

    public VolumePlugin(PluginManager manager, int sampleRate) {
        super("volume");
        m_sampleRate = sampleRate;
        m_graphData = new ArrayList<Pair<Number, Number>>();
    }

    /**
     * We're assuming each "sample" is 2 bytes
     * @param bytes
     * @return
     */
    private double rmsPerSample(byte[] bytes) {
        short sample = 0;
        double[] rmsValues = new double[bytes.length / 2];
        double meanSquared = 0;
        for (int i = 0; i < bytes.length; i += 2) {
            sample = (short)((bytes[i+1] << 8) | bytes[i]);
            meanSquared +=  sample*sample;
        }

        meanSquared = meanSquared/(bytes.length/2);

        return Math.sqrt(meanSquared);
    }

    @Override
    protected void processChunk(byte[] chunk) {

        for (int i = 0; i < 4; ++i) {
            byte[] part = new byte[chunk.length/4];
            System.arraycopy(chunk, part.length*i, part, 0, part.length);

            double rmsValue = rmsPerSample(part);

            int ms = (_manager.getChunkNum()*1000) + i*250;
            double decibels = 0;
            if (rmsValue != 0) {
                decibels = 20 * Math.log10(rmsValue);

                //guess that they are not talking if their voice is below 40 db
                if (decibels > 40) {
                    m_volumeSum += decibels;
                    m_volumeCount++;
                }

                m_graphData.add(new Pair<Number, Number>((float) ms / (float) 1000, decibels));
                TimeRange tr = new TimeRange(ms, ms + 250);
                if (timeRangeExists(tr))
                    _feedback.get(tr).add(new VolumeFeedback(decibels));
                else {
                    List<Feedback> newFeed = new ArrayList<>();
                    newFeed.add(new VolumeFeedback(decibels));
                    _feedback.put(new TimeRange(ms, (ms + 250)), newFeed);
                }
            }
        }
    }

    @Override
    protected void finalize()
    {
        if(m_volumeCount == 0)
        {
            m_quickFeedback = "I couldn't detect anything.";
            percentScore = 0;
        }
        double averageVolume = m_volumeSum/m_volumeCount;

        percentScore -= Math.abs(70-averageVolume);

        if(averageVolume <= 75 && averageVolume >= 65)
            m_quickFeedback = "Your volume was perfect!";

        if(averageVolume > 75)
            m_quickFeedback = "You spoke very loudly, try lowering your voice";

        if(averageVolume  < 65 && averageVolume > 50)
            m_quickFeedback = "You spoke a little quietly, try raising your voice slightly";

        if(averageVolume  <= 50)
            m_quickFeedback = "You spoke very quietly, try raising your voice";
    }

    @Override
    public Object getData() {
        return m_graphData;
    }

    @Override
    public GraphType getType() {
        return GraphType.xy_scatter;
    }

    @Override
    public void ChangeGraphLook(XYPlot plot) {
        ArrayList<Number> xVals = new ArrayList<>();
        ArrayList<Number> yVals = new ArrayList<>();
        for(Pair<Number, Number> pair : m_graphData)
        {
            xVals.add(pair.first);
            yVals.add(pair.second);
        }
        plot.setMinimumWidth(1000);
        // Turn the above arrays into XYSeries':
        XYSeries series1 = new SimpleXYSeries(
                xVals,
                yVals,
                "Volume Data");

        SCPlotManager.getInstance().addSeries(series1, Color.rgb(0,255,0));
    }


    @Override
    public String getXMLTag() {
        String result = "";
        result += "\t<Volume>\n";
        for (Pair<Number, Number> dataPoint : m_graphData) {
            result += "\t\t<VolumeData>\n";

            float x = dataPoint.first.floatValue();
            float y = dataPoint.second.floatValue();

            result += "\t\t\t<x>" + Float.toString(x) + "</x>\n";
            result += "\t\t\t<y>" + Float.toString(y) + "</y>\n";

            result += "\t\t</VolumeData>\n";
        }
        result += "\t\t<score>" + Integer.toString(percentScore) + "</score>\n";
        result += "\t\t<quickFeedback>" + m_quickFeedback + "</quickFeedback>\n";
        result += "\t</Volume>\n";

        return result;
    }

    @Override
    //feedback
    public void readXMLTag() {

        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();
        Element volumeElem = (Element)(doc.getElementsByTagName("Volume").item(0));

        NodeList nList = volumeElem.getElementsByTagName("VolumeData");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                float x = Float.parseFloat(elem.getElementsByTagName("x").item(0).getTextContent());
                float y = Float.parseFloat(elem.getElementsByTagName("y").item(0).getTextContent());
                long test = (long) x;
                m_graphData.add(new Pair(x, y));
                TimeRange tr = new TimeRange((long) (x * 1000), (long) ((x * 1000) + 250));

                _feedback.put(tr, new ArrayList<Feedback>());
                _feedback.get(tr).add(new VolumeFeedback(y));
            }
        }
        percentScore = Integer.parseInt(volumeElem.getElementsByTagName("score").item(0).getTextContent());
        m_quickFeedback = volumeElem.getElementsByTagName("quickFeedback").item(0).getTextContent();
    }

}
