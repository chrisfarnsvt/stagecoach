package test.stagecoach.Analysis;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.Graphable;
import test.stagecoach.Utils.XMLable;

/**
 * Created by colin on 11/6/2015.
 */
public abstract class Plugin implements Runnable, Graphable, XMLable {

    protected List<String> _dependencyIDs;
    protected String _ID;
    protected Map<TimeRange, List<Feedback>> _feedback;
    protected PluginManager _manager;
    protected Feedback _feedbackType;
    protected byte[] m_currentChunk;
    protected int percentScore;
    private boolean m_bIsFinished;
    protected String m_quickFeedback;

    public Plugin(String ID) {
        _manager = PluginManager.getInstance();
        _ID = ID;
        _feedback = new HashMap<>();
        _dependencyIDs = new ArrayList<String>();
        m_bIsFinished = false;
        percentScore = 100;
        m_quickFeedback = "";
    }

    public Map<TimeRange, List<Feedback>> getFeedback() {
        return _feedback;

    }

    public void run()
    {
        m_currentChunk = _manager.getChunk();
        processChunk(m_currentChunk);
    }

    public int getPercentScore()
    {
        return percentScore;
    }


    protected abstract void finalize(); // finish any leftover processing once you've seen the whoel file

    protected abstract void processChunk(byte[] chunk);

    public String getID() { return _ID;}

    public boolean equals(Plugin other) {
        return other.getID().equals(getID());
    }

    public List<String> getDependencies() { return _dependencyIDs;}

    public boolean timeRangeExists(TimeRange tr) {
        return _feedback.containsKey(tr);
    }

    public void setFinished(boolean isFinished)
    {
        m_bIsFinished = isFinished;
    }
    public boolean isFinished() { return m_bIsFinished; }
    public boolean isTranscriptPlugin() { return false;}
    public String getQuickFeedback()
    {
        return m_quickFeedback;
    }
}
