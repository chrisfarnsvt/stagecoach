package test.stagecoach.Analysis.Plugins;

import android.util.Pair;

import java.util.List;

import test.stagecoach.Analysis.Feedback;

/**
 * Created by chris_000 on 3/15/2016.
 */
public class WordOveruseFeedback implements Feedback {

    private Pair<String, Integer> _wordAndCount;

    public WordOveruseFeedback(String word, int wordCount) {
        _wordAndCount = new Pair<>(word, wordCount);
    }

    public Pair<String, Integer> getWordCount() {
        return _wordAndCount;
    }

    @Override
    public String getInfo() {
        return _wordAndCount.first + " appears " + _wordAndCount.second + " times.";
    }

    @Override
    public int getDeduction() {
        return 0;
    }
}
