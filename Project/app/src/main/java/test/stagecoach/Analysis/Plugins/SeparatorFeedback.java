package test.stagecoach.Analysis.Plugins;

import test.stagecoach.Analysis.Feedback;

/**
 * Created by colin on 4/3/2016.
 */
public class SeparatorFeedback implements Feedback {

    private int m_word;

    public SeparatorFeedback(int word)
    {
        m_word = word;
    }

    @Override
    public String getInfo() {
        return "word " + m_word + " is a separtator.";
    }

    public int getWordNum()
    {
        return  m_word;
    }

    @Override
    public int getDeduction() {
        return 0;
    }
}
