package test.stagecoach.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.androidplot.Plot;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.XLayoutStyle;
import com.androidplot.ui.YLayoutStyle;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.StageCoach;
import test.stagecoach.UI.GraphClickListener;
import test.stagecoach.UI.GraphFragment;

/**
 * Created by colin on 2/7/2016.
 */
public class SCPlotManager {

    static SCPlotManager m_instance;

    static public void initialize(XYPlot plot, GraphFragment graphFragment)
    {
        m_instance = new SCPlotManager(plot, graphFragment);
    }

    public static SCPlotManager getInstance()
    {
        return m_instance;
    }

    private XYPlot m_plot;
    private PluginManager m_pm;
    private GraphFragment m_graphFragment;
    private String m_currentPluginID;
    private List<XYSeries> m_currentSeries;

    public SCPlotManager(XYPlot plot, GraphFragment graphFragment)
    {
        m_plot = plot;
        m_pm = PluginManager.getInstance();
        m_graphFragment = graphFragment;
        m_currentPluginID = "";
        m_currentSeries = new ArrayList();
    }


    public void initializeGraph(Plugin plugin)
    {
        for(XYSeries s : m_currentSeries)
        {
            m_plot.removeSeries(s);
        }

        m_plot.clear();
        m_plot.invalidate();

        m_currentPluginID = plugin.getID();
        m_plot.getRangeLabelWidget().getLabelPaint().setTextSize(PixelUtils.dpToPix(7));

        //m_plot.getDomainLabelWidget().setMargins(100, 100, 100, 100);

        m_plot.getDomainLabelWidget().getLabelPaint().setTextSize(PixelUtils.dpToPix(7));

        m_plot.getTitleWidget().setSize(new SizeMetrics(20, SizeLayoutType.ABSOLUTE, 0, SizeLayoutType.FILL));
        m_plot.getTitleWidget().getLabelPaint().setTextSize(PixelUtils.dpToPix(12));

        m_plot.setOnClickListener(new GraphClickListener(m_graphFragment));

        // Domain
        // m_plot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, days.length);
        //m_plot.setDomainValueFormat(new DecimalFormat("0"));
        //m_plot.setDomainStepValue(1);

        //Range
        m_plot.setRangeBoundaries(0, 100, BoundaryMode.FIXED);
        m_plot.setRangeStepValue(10);
        m_plot.setRangeStep(XYStepMode.SUBDIVIDE, 10);
        m_plot.setRangeValueFormat(new DecimalFormat("0"));

        //let the plugin mess with the graph
        plugin.ChangeGraphLook(m_plot);

        if(plugin.getType() == GraphType.xy_scatter) {
            // reduce the number of range labels
            m_plot.setTicksPerRangeLabel(3);
            m_plot.getGraphWidget().setDomainLabelOrientation(-45);

            XYGraphWidget graphWidget = m_plot.getGraphWidget();
            SizeMetrics sm = new SizeMetrics(30, SizeLayoutType.FILL, 0, SizeLayoutType.FILL);
            graphWidget.setSize(sm);
            graphWidget.position(0, XLayoutStyle.ABSOLUTE_FROM_LEFT, 30, YLayoutStyle.ABSOLUTE_FROM_TOP);

            m_plot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
            m_plot.setBackgroundColor(Color.WHITE);
            m_plot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
            m_plot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);

            m_plot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
            m_plot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

            m_plot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
            m_plot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
            m_plot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);

            m_plot.getLayoutManager().remove(m_plot.getLegendWidget());
            m_plot.getLayoutManager().remove(m_plot.getDomainLabelWidget());
            m_plot.getLayoutManager().remove(m_plot.getRangeLabelWidget());
            m_plot.setPlotMargins(0, 0, 0, 0);

            // float curDomH = m_plot.getDomainLabelWidget().getHeightPix();
            //m_plot.getDomainLabelWidget().setSize()
        }
    }

    public XYPlot getPlot()
    {
        return m_plot;
    }


    public String getCurPluginID()
    {
        return m_currentPluginID;
    }

    public void addSeries(XYSeries newSeries, int color)
    {
        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml:
        LineAndPointFormatter seriesFormat = new LineAndPointFormatter(color,
                null,
                null,
                null);

        Paint lp = seriesFormat.getLinePaint();
        lp.setStrokeWidth(5);
        seriesFormat.setLinePaint(lp); /*
        seriesFormat.setFillPaint(null);
        seriesFormat.setVertexPaint(null);
        seriesFormat.setFillDirection(FillDirection.BOTTOM);
        seriesFormat.setPointLabelFormatter(null);
        */

        m_plot.addSeries(newSeries, seriesFormat);
        m_currentSeries.add(newSeries);

        Number maxX = newSeries.getX(newSeries.size()-1);

        WindowManager wm = (WindowManager) StageCoach.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = size.y;
        Resources r = StageCoach.getContext().getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 400, r.getDisplayMetrics());

        float tenSecChunks = maxX.floatValue()/10f;
        int newPixCount  = (int)(width*tenSecChunks);

        ViewGroup.LayoutParams params = m_plot.getLayoutParams();
        params.height = 750;
        if(newPixCount < width)
            newPixCount = width;

        //params.width = newPixCount;
        m_plot.setLayoutParams(params);
        m_plot.redraw();
        int test = 0;
    }

}
