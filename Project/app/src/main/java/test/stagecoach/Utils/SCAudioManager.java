package test.stagecoach.Utils;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by colin on 10/30/2015.
 *
 * StageCoachAudioManager - records, store and plays PCM audio data
 */

public class SCAudioManager {

    //THe audio manager is a singleton so you dont construct it you simply request the instance
    private static SCAudioManager m_instance;

    private static final int RECORDER_SAMPLERATE = 16000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private AudioRecord m_recorder = null;
    private Thread m_recordingThread = null;
    private boolean m_isRecording = false;
    private boolean m_isPlaying = false;
    private String m_filename;
    private String _wavFileName;
    private int m_bytesRead;
    private SCFileManager m_fm;

    public int getRecorderSamplerate() {return RECORDER_SAMPLERATE;}

    public static SCAudioManager getInstance() {
        if(m_instance == null)
            m_instance = new SCAudioManager();

        return m_instance;
    }

    public static void reset()
    {
        m_instance = new SCAudioManager();
    }

    private SCAudioManager()
    {
        m_fm = SCFileManager.getInstance();
        //set up filename
     /*   String fileNamePrefix = "/voice16bitmono";
        _wavFileName = m_filename + fileNamePrefix + ".wav";
        //SERIOUSLY, DO IT

        try {
            (new File(_wavFileName)).createNewFile();
        } catch (IOException e) {
            Log.d("DEBUG", "Creating wav failed");
        }
        m_filename += (fileNamePrefix + ".pcm");*/
    }


    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format

    //used to reset file location on review
    public void updateFileLocation()
    {
        String fileNamePrefix = "/voice16bitmono";
        m_filename = m_fm.getAttemptDir() + (fileNamePrefix + ".pcm");
    }

    public void startRecording() {

        String fileNamePrefix = "/voice16bitmono";
        m_filename = m_fm.getAttemptDir() + (fileNamePrefix + ".pcm");
        _wavFileName =  m_fm.getAttemptDir() + fileNamePrefix + ".wav";
        //SERIOUSLY, DO IT

        try {
            (new File(_wavFileName)).createNewFile();
        } catch (IOException e) {
            Log.d("DEBUG", "Creating wav failed");
        }

        if (!m_isRecording) {
            m_isRecording = true;
            m_recorder = createRecorder();
            if (m_recorder == null) {
                return;
            }
            m_recorder.startRecording();
            m_recordingThread = new Thread(new Runnable() {
                public void run() {
                    writeAudioDataToFile();
                }
            }, "AudioRecorder Thread");
            m_recordingThread.start();
        }

    }

    private static int[] mSampleRates = new int[]{16000/*, 22050, 11025, 8000*/};

    private AudioRecord createRecorder() {
        /*for (int rate : mSampleRates) {
            try {
                Log.d("DEBUG", "Attempting rate " + rate + "Hz, bits: ");
                int bufferSize = AudioRecord.getMinBufferSize(rate, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

                if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                    // check if we can instantiate and have a success
                    AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, rate, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);

                    if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
                        //RECORDER_SAMPLERATE = rate;
                        return recorder;
                    }
                }
            } catch (Exception e) {
                Log.e("DEBUG", rate + "Exception, keep trying.", e);
            }
        }*/

        return new AudioRecord(MediaRecorder.AudioSource.DEFAULT, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING));
    }

    //convert short to byte
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    private void writeAudioDataToFile() {
        // Write the output audio in bytes

        //GET RID OF THIS FOR FINAL BUILD

        try {
            (new File(m_filename)).createNewFile();
        } catch (IOException e) {
            Log.d("DEBUG", "Creating pcm failed");
        }
        short sData[] = new short[BufferElements2Rec];

        FileOutputStream os = null;
        try {
            os = new FileOutputStream(m_filename, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (m_isRecording) {
            // gets the voice output from microphone to byte format

            m_recorder.read(sData, 0, BufferElements2Rec);
            try {
                // // writes the data to file from buffer
                // // stores the voice buffer
                byte bData[] = short2byte(sData);
                os.write(bData, 0, BufferElements2Rec * BytesPerElement);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            os.close();
            Log.d("DEBUG", "wrote to: " + m_filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopRecording() {
        m_isRecording = false;
        // stops the recording activity
        if (null != m_recorder) {
            m_recorder.stop();
            m_recorder.release();
            m_recorder = null;
            m_recordingThread = null;
        }
    }

    public String getRecordingLocation() {
        return m_fm.getAttemptDir() + "/voice16bitmono.pcm";
    }

    boolean paused = false;

    public boolean isPaused(){return paused;}

    public void playRecordedAudio() throws IOException {
        m_isPlaying = true;
        // We keep temporarily filePath globally as we have only two sample sounds now..
        if (m_filename == null)
            return;

        int intSize = android.media.AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT);

        AudioTrack audioPlayer = new AudioTrack(AudioManager.STREAM_MUSIC, RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, intSize, AudioTrack.MODE_STREAM, 12345);

        int count = 1024; // 1 kb
        //Reading the file..
        byte[] byteData = null;
        File file = null;
        file = new File(m_filename);

        byteData = new byte[(int) count];
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        FileChannel fc = in.getChannel();
        int ret = 0;
        m_bytesRead = 0;
        int size = (int) file.length();

        audioPlayer.play();
        while (m_bytesRead < size) {
            if(paused)
                continue;
            fc.position(m_bytesRead);
            ret = in.read(byteData, 0, count);
            //Log.d("DEBUG", String.valueOf(ret));
            if (ret != -1) {
                // Write the byte array to the track
                audioPlayer.write(byteData, 0, ret);
                m_bytesRead += ret;
            } else
                break;

        }
        in.close();
        audioPlayer.stop();
        audioPlayer.release();
        m_isPlaying = false;
    }

    public void pausePlayback()
    {
        paused = true;
    }

    public void resumePlayback()
    {
        paused = false;
    }

    public String getWavFileName() {
        return m_fm.getAttemptDir() + "/voice16bitmono.wav";
    }

    public boolean isRecording(){return m_isRecording;}

    public boolean isPlaying(){return m_isPlaying;}

    public int getBytesRead(){return m_bytesRead;}

    public int getDurationMS(){
        File audio = new File(m_filename);
        int duration =  (int)((float)audio.length()/(float)RECORDER_SAMPLERATE * 1000);
        return duration;
    }

    public int getPositionMS()
    {
        return (int)((float)m_bytesRead/(float)RECORDER_SAMPLERATE * 1000);
    }

    public void seekTo(float ms)
    {
        File audio = new File(m_filename);
        float bytesPerMs = audio.length()/getDurationMS();

        int seekByte = (int)(bytesPerMs*ms);
        m_bytesRead = seekByte;
    }

}