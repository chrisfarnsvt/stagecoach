package test.stagecoach.Utils;

import java.io.IOException;

/**
 * Created by colin on 11/22/2015.
 */
public class AudioRunnable implements Runnable {
    SCAudioManager m_am;

    public AudioRunnable(SCAudioManager am) {
        m_am = am;
    }

    public void run() {
        try {
            m_am.playRecordedAudio();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isPaused() {
        return m_am.isPaused();
    }

    public void pause() {
        m_am.pausePlayback();
    }

    public void resume() {
        m_am.resumePlayback();
    }
}

