package test.stagecoach.Utils;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Decoder;
import edu.cmu.pocketsphinx.Config;
import edu.cmu.pocketsphinx.Segment;

/**
 * wrapper for speech recognition. contains default settings to make api easier to call for our
 * purposes.
 * Created by chris_000 on 11/3/2015.
 */
public class STTManager implements Runnable, XMLable{

    private String _fileName;
    private String _wavFileName;
    private ArrayList<String> _results;
    private Context _context;
    private Map<Integer, Pair<Integer, Integer>> _segmentWithTimestamp;
    private Decoder _decoder = null;
    private static Thread _initializeThread;
    static STTManager _sttManager;

    public static STTManager getInstance(Context context)
    {
        if(_sttManager == null)
            _sttManager = new STTManager(context);

        return _sttManager;
    }

    public void reset()
    {
        _results.clear();
        _segmentWithTimestamp.clear();
    }
    //USE AT YOUR OWN RISK, HOMIE
    public static STTManager getInstance()
    {
        if(_sttManager == null)
            _sttManager = new STTManager(null);

        return _sttManager;
    }

    public static void initialize(Context context)
    {
        if(_sttManager == null)
            _sttManager = new STTManager(context);
        _initializeThread = new Thread(_sttManager);
        _initializeThread.start();
    }

    private STTManager(Context context) {

        _context = context;

        _segmentWithTimestamp = new HashMap<>();
        _results = new ArrayList<>();
    }

    static {
        System.loadLibrary("pocketsphinx_jni");
    }

    //initializes decoder
    public void run()
    {
        Config c = Decoder.defaultConfig();
        Assets assets = null;
        File assetDir = null;
        try {
            assets = new Assets(_context);
            assetDir = assets.syncAssets();
            Log.d("DEBUG", assetDir.toString());
        } catch (IOException e) {
            Log.d("DEBUG", "Assets couldn't be created");
            e.printStackTrace();
            return;
        }

        c.setString("-hmm", new File(assetDir, "/en-us").toString());
        c.setString("-lm", new File(assetDir, "en-us.lm.dmp").toString());
        c.setString("-dict", new File(assetDir, "cmudict-en-us.dict").toString());
        c.setBoolean("-addFillerWords", true);

        //c.setBoolean("-fsgusefiller", true);
//        c.setBoolean("-fwdflat", false);
//        c.setBoolean("-backtrace", true);

        _decoder = new Decoder(c);
    }

    public void translate(String file, String wavFile) {

        _fileName = file;
        _wavFileName = wavFile;

        if (file != null)
            pcmToWav();

        if(_initializeThread != null) {
            try {
                _initializeThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            _initializeThread = null;
        }
        long start = SystemClock.currentThreadTimeMillis();
        FileInputStream stream = null;
        URI testwav = null;
        try {
            testwav = new URI("file:" + _wavFileName);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.d("DEBUG", "URI borked");
            return;
        }
        try {
            stream = new FileInputStream(new File(testwav));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("DEBUG", "File stream borked");
            return;
        }
        _decoder.startUtt();
        byte[] b = new byte[4096];
        try {
            int nbytes;
            while ((nbytes = stream.read(b)) >= 0) {
                ByteBuffer bb = ByteBuffer.wrap(b, 0, nbytes);

                // Not needed on desktop but required on android
                bb.order(ByteOrder.LITTLE_ENDIAN);

                short[] s = new short[nbytes/2];
                bb.asShortBuffer().get(s);
                _decoder.processRaw(s, nbytes/2, false, false);
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("DEBUG", "borked");
            return;
        }
        _decoder.endUtt();
        int i = 0;
        for (Segment seg : _decoder.seg()) {
            String test = seg.getWord();
            if (!seg.getWord().contains("<")) {
                _segmentWithTimestamp.put(i, new Pair<>(seg.getStartFrame() * 10, seg.getEndFrame() * 10));
                Log.d("DEBUG", "start is " + seg.getStartFrame()  + " end is " + seg.getEndFrame() + " middle is " + (seg.getEndFrame() - (seg.getStartFrame() / 2)));
                _results.add(seg.getWord());
                i++;
            }
        }

        long end = SystemClock.currentThreadTimeMillis();
        Log.d("Time", "translating took " + (end - start) + "ms.");
    }

    public Map<Integer, Pair<Integer, Integer>> getSegments() { return _segmentWithTimestamp; }

    public ArrayList<String> getWords() {
        return _results;
    }

    private void pcmToWav()
    {
        try {
            long mySubChunk1Size = 16;
            int myBitsPerSample= 16;
            int myFormat = 1;
            long myChannels = 1;
            long mySampleRate = SCAudioManager.getInstance().getRecorderSamplerate();
            long myByteRate = mySampleRate * myChannels * myBitsPerSample/8;
            int myBlockAlign = (int) (myChannels * myBitsPerSample/8);
            File audio = new File(_fileName);

            long myDataSize = audio.length();
            long myChunk2Size =  myDataSize * myChannels * myBitsPerSample/8;
            long myChunkSize = 36 + myChunk2Size;

            OutputStream os;
            os = new FileOutputStream(new File(_wavFileName));
            BufferedOutputStream bos = new BufferedOutputStream(os);
            DataOutputStream outFile = new DataOutputStream(bos);

            outFile.writeBytes("RIFF");                                 // 00 - RIFF
            outFile.write(intToByteArray((int) myChunkSize), 0, 4);      // 04 - how big is the rest of this file?
            outFile.writeBytes("WAVE");                                 // 08 - WAVE
            outFile.writeBytes("fmt ");                                 // 12 - fmt
            outFile.write(intToByteArray((int) mySubChunk1Size), 0, 4);  // 16 - size of this chunk
            outFile.write(shortToByteArray((short) myFormat), 0, 2);     // 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
            outFile.write(shortToByteArray((short) myChannels), 0, 2);   // 22 - mono or stereo? 1 or 2?  (or 5 or ???)
            outFile.write(intToByteArray((int) mySampleRate), 0, 4);     // 24 - samples per second (numbers per second)
            outFile.write(intToByteArray((int) myByteRate), 0, 4);       // 28 - bytes per second
            outFile.write(shortToByteArray((short) myBlockAlign), 0, 2); // 32 - # of bytes in one sample, for all channels
            outFile.write(shortToByteArray((short) myBitsPerSample), 0, 2);  // 34 - how many bits in a sample(number)?  usually 16 or 24
            outFile.writeBytes("data");                                 // 36 - data
            outFile.write(intToByteArray((int)myDataSize), 0, 4);       // 40 - how big is this data chunk

            int CHUNK_SIZE = 1048576;
            byte[] chunk = new byte[CHUNK_SIZE];
            BufferedInputStream stream = new BufferedInputStream(new FileInputStream(audio));
            for(long l = 0; l < audio.length(); l+= CHUNK_SIZE) {
                stream.read(chunk, 0, CHUNK_SIZE);
                outFile.write(chunk);                                    // 44 - the actual data itself - just a long string of numbers
            }
            outFile.flush();
            outFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static byte[] intToByteArray(int i)
    {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    // convert a short to a byte array
    public static byte[] shortToByteArray(short data)
    {
        /*
         * NB have also tried:
         * return new byte[]{(byte)(data & 0xff),(byte)((data >> 8) & 0xff)};
         *
         */

        return new byte[]{(byte)(data & 0xff),(byte)((data >>> 8) & 0xff)};
    }

    @Override
    public String getXMLTag()
    {
        String result = "";
        int counter = 0;
        for (String word : _results) {
            result += "\t<transcriptWord>\n";

            int wordID = counter;
            counter++;
            int startFrame = _segmentWithTimestamp.get(wordID).first;
            int endFrame = _segmentWithTimestamp.get(wordID).second;

            result += "\t\t<wordText>" + word + "</wordText>\n";
            result += "\t\t<wordID>" + wordID + "</wordID>\n";
            result += "\t\t<startFrame>" + startFrame + "</startFrame>\n";
            result += "\t\t<endFrame>" + endFrame + "</endFrame>\n";

            result += "\t</transcriptWord>\n";
        }

        return result;
    }

    @Override
    public void readXMLTag()
    {
        File inputFile = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("transcriptWord");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                String word = elem.getElementsByTagName("wordText").item(0).getTextContent();
                int wordID = Integer.parseInt(elem.getElementsByTagName("wordID").item(0).getTextContent());
                int startFrame = Integer.parseInt(elem.getElementsByTagName("startFrame").item(0).getTextContent());
                int endFrame = Integer.parseInt(elem.getElementsByTagName("endFrame").item(0).getTextContent());
                _results.add(word);
                _segmentWithTimestamp.put(wordID, new Pair<Integer, Integer>(startFrame, endFrame));
            }
        }
    }
}