package test.stagecoach.Utils;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;

/**
 * Created by chris_000 on 3/4/2016.
 */
public class XMLifier {
    static XMLifier _XMLifier;
    private String _docText = "";

    public static XMLifier getInstance()
    {
        if(_XMLifier == null)
            _XMLifier = new XMLifier();

        return _XMLifier;
    }

    private XMLifier ()
    {

    }

    public void reset()
    {
        _docText = "";
    }

    public String getDocumentText(){ return _docText; }

    public void buildOutputString()
    {
        _docText += "<?xml version=\"1.0\"?>\n";
        _docText += "<results>\n";
        _docText += STTManager.getInstance().getXMLTag();
        for(Plugin p : PluginManager.getInstance().getPlugins())
        {
            _docText += p.getXMLTag();
        }
        _docText += "<avgScore>" + PluginManager.getInstance().getAverageScore() + "</avgScore>";
        _docText += "</results>\n";
    }
}
