package test.stagecoach.Utils;

/**
 * Created by colin on 1/29/2016.
 */
public enum GraphType {
    xy_scatter,
    transcript,
    none
}
