package test.stagecoach.Utils;

/**
 * Created by chris_000 on 3/4/2016.
 */
public interface XMLable {
    public abstract String getXMLTag();
    public abstract void readXMLTag();
}
