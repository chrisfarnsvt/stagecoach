package test.stagecoach.Utils;

import com.androidplot.xy.XYPlot;

/**
 * Created by colin on 1/29/2016.
 */

public interface Graphable {

    public abstract Object getData();
    public abstract GraphType getType();
    public abstract void ChangeGraphLook(XYPlot plot);
}
