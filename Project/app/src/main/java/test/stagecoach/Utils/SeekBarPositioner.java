package test.stagecoach.Utils;

import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by colin on 11/22/2015.
 */
public class SeekBarPositioner implements Runnable, SeekBar.OnSeekBarChangeListener{

    SeekBar m_seekBar;
    TextView m_position;
    TextView m_duration;
    SCAudioManager m_audioManager;

    public SeekBarPositioner(SeekBar sb, TextView pos, TextView dur)
    {
        m_seekBar = sb;
        m_seekBar.setOnSeekBarChangeListener(this);
        m_position = pos;
        m_duration = dur;
        m_audioManager = SCAudioManager.getInstance();
    }

    boolean paused = false;

    public void pause()
    {
        paused = true;
    }

    public void resume()
    {
        paused = false;
    }

    public boolean isPaused() { return paused; }

    public void run() {
        final SCAudioManager am = SCAudioManager.getInstance();

        boolean started = false;
        while (started == false) {
            while (am.isPlaying()) {
                if(paused)
                    continue;
                started = true;
                int mediaPos_new = am.getPositionMS();
                final int totalSeconds = mediaPos_new/1000;
                m_position.post(new Runnable() {
                    public void run() {
                        int minutes = totalSeconds / 60;
                        int seconds = totalSeconds % 60;
                        String dur;
                        if(seconds < 10)
                            dur = Integer.toString(minutes) + ":0" + Integer.toString(seconds);
                        else
                            dur = Integer.toString(minutes) + ":" + Integer.toString(seconds);

                        m_position.setText(dur);
                    }
                });
                int mediaMax_new = am.getDurationMS();
                m_seekBar.setMax(mediaMax_new);
                m_seekBar.setProgress(mediaPos_new);

                try {
                    Thread.sleep(10);
                } catch (java.lang.InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if(b)
        {
            float position = (float)i / (float)seekBar.getMax();
            float ms = position * m_audioManager.getDurationMS();

            final int totalSeconds = (int)ms/1000;
            m_position.post(new Runnable() {
                public void run() {
                    int minutes = totalSeconds / 60;
                    int seconds = totalSeconds % 60;
                    String dur;
                    if(seconds < 10)
                        dur = Integer.toString(minutes) + ":0" + Integer.toString(seconds);
                    else
                        dur = Integer.toString(minutes) + ":" + Integer.toString(seconds);

                    m_position.setText(dur);
                }
            });

            m_audioManager.seekTo(ms);
        }
    }

    private boolean wasPaused;

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        wasPaused = paused;
        pause();
        m_audioManager.pausePlayback();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if(!wasPaused)
            resume();
        m_audioManager.resumePlayback();
    }
};