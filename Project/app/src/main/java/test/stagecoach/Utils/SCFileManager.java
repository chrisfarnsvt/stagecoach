package test.stagecoach.Utils;

import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by colin on 11/25/2015.
 *
 * Singleton for file operations in stagecoach
 */
public class SCFileManager {

    static SCFileManager m_instance;

    public static SCFileManager getInstance(File root)
    {
        if(m_instance == null)
            m_instance = new SCFileManager(root);

        return m_instance;
    }

    public static SCFileManager getInstance()
    {
        return m_instance;
    }


    private File m_speechDir; // the speech of the file we are currently looking at
    private File m_rootDir; //the root dir of the app, only contents should be speech dirs
    private File m_attemptDir;

    private SCFileManager(File root)
    {
        m_rootDir = root;
    }

    public File[] getSpeechDirs()
    {
       return m_rootDir.listFiles();
    }

    public void setSpeechDir(File speechDir)
    {
        m_speechDir = speechDir;
    }

    public void setSpeechDir(String dirName)
    {
        for(File f : m_rootDir.listFiles())
        {
            if(f.getName().equals(dirName))
                m_speechDir = f;
        }
    }

    public String getSpeechName()
    {
        return m_speechDir.getName();
    }


    public String getAttemptDir()
    {
        return m_attemptDir.getPath();
    }

    public void setAttemptDir(String attemptDir)
    {
        for(File f : m_speechDir.listFiles())
        {
            if(f.getName().equals(attemptDir))
                m_attemptDir = f;
        }
    }

    public File[] getSpeechResults()
    {
        //TODO: implement once result format is defined
        return null;
    }

    public void createSpeechDir(String name)
    {
        m_speechDir = new File(m_rootDir.getPath() + "/" + name);
        m_speechDir.mkdir();
    }


    public void createAttemptDir()
    {
        //Attempts are named Attempt1, Attempt2, etc
        int attemptNum = 1;
        File[] attempts = m_speechDir.listFiles();
        if(attempts != null)
        {
            for (File f : m_speechDir.listFiles())
            {
                if (f.isDirectory() && f.getName().contains("Attempt"))
                    attemptNum++;
            }
        }

        m_attemptDir = new File(m_speechDir.getPath()+"/Attempt" + attemptNum);
        m_attemptDir.mkdir();
    }

    public File[] getAttempts()
    {
        //TODO: check validity of files
        return m_speechDir.listFiles();
    }

    public void deleteCurrentSpeech()
    {
        recursiveDelete(m_speechDir);
        m_speechDir = null;
        m_attemptDir = null;
    }

    public void deleteCurrentAttempt()
    {
        recursiveDelete(m_attemptDir);
        m_attemptDir = null;
    }

    private void recursiveDelete(File dir)
    {
        String[]entries = dir.list();
        for(String s: entries){
            File currentFile = new File(dir.getPath(),s);
            if(currentFile.isDirectory())
                recursiveDelete(currentFile);
            else
                currentFile.delete();
        }
        dir.delete();
    }

    public static boolean doesXMLExist() {
        File check = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        return check.exists();
    }

    /**
     * Writes the xml string to a file in the same file location as the
     * @param str
     */
    public static void writeSpeechXMLData(String str)
    {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(str)));

            TransformerFactory tFact = TransformerFactory.newInstance();
            Transformer t = tFact.newTransformer();

            DOMSource source = new DOMSource(doc);

            StreamResult sResult = new StreamResult(new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml"));
            t.transform(source, sResult);

        } catch (ParserConfigurationException e) {
            Log.d("DEBUG", "Configuring the doc parser broke");
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public File getXMLFile()
    {
        File xml = new File(SCFileManager.getInstance().getAttemptDir() + "/speechData.xml");
        return xml;
    }


}
