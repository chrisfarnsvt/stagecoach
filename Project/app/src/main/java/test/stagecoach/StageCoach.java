package test.stagecoach;

import android.app.Application;
import android.content.Context;

/**
 * Created by colin on 2/7/2016.
 *
 * provides a static way to access app-wide things like context
 */
public class StageCoach extends Application {

    private static Context sContext;
    @Override
    public void onCreate() {
        sContext = getApplicationContext();
        super.onCreate();
    }

    public static Context getContext() {
        return sContext;
    }
}
