package test.stagecoach.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.stagecoach.Analysis.Feedback;
import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Analysis.Plugins.PausesPlugin;
import test.stagecoach.Analysis.Plugins.SeparatorFeedback;
import test.stagecoach.Analysis.Plugins.SeparatorPlugin;
import test.stagecoach.Analysis.Plugins.VolumeFeedback;
import test.stagecoach.Analysis.Plugins.VolumePlugin;
import test.stagecoach.Analysis.Plugins.WordOveruseFeedback;
import test.stagecoach.Analysis.TimeRange;
import test.stagecoach.R;
import test.stagecoach.StageCoach;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;
import test.stagecoach.Utils.XMLifier;

public class TranscriptFragment extends Fragment {

    private ArrayList<String> _transcriptResults;
    private ArrayList<TranscriptWordView> _words;
    private SpinnerFragment m_SpinnerFragment;
    private View _rootView;
    private LinearLayout _layout;
    private STTManager sttm;
    private Map<Integer, Double> _volumeAtWordNum;
    private List<Pair<List<Feedback>, Integer>> _FeedbackToWordID;
    private List<Pair<Integer, Integer>> _afterWordToTime;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.

        _FeedbackToWordID = new ArrayList<>();
        _transcriptResults = new ArrayList<>();
        _afterWordToTime = new ArrayList<>();
        _words = new ArrayList<>();
        sttm = STTManager.getInstance();

        View _rootView = inflater.inflate(
                R.layout.fragment_transcript, container, false);

        ProgressBar progressBar = new ProgressBar(container.getContext());
        if (container instanceof FrameLayout) {
            FrameLayout.LayoutParams layoutParams =
                    new FrameLayout.LayoutParams(50, 50, Gravity.CENTER);
            progressBar.setLayoutParams(layoutParams);
        }

        _layout = (LinearLayout) _rootView.findViewById(R.id.transcriptContainer);


        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        boolean makeTranscript = SP.getBoolean("transcriptGen", true);

        if(makeTranscript) {

            _transcriptResults = sttm.getWords();

            LinearLayout hLayout = new LinearLayout(StageCoach.getContext());
            hLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hLayout.setOrientation(LinearLayout.HORIZONTAL);

            _volumeAtWordNum = getVolumesPerWord();
            addFeedbacks();

            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

            final int TOTAL_WIDTH = metrics.widthPixels - 40;
            int currentRemainingWidth = TOTAL_WIDTH;

            for (int i = 0; i < _transcriptResults.size(); i++) {
                String s = _transcriptResults.get(i);
                TranscriptWordView word = new TranscriptWordView(_layout.getContext(), s, _FeedbackToWordID.get(i).first);
                _words.add(word);
                TranscriptWordView pause = null;
               if(_afterWordToTime.size() > 0) {
                   if(_afterWordToTime.get(0).first <= i) {
                        pause = addPause();
                   }
               }
                word.display();
                if (s.contains("("))
                    s = truncateWord(s);

                word.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                int wordWidth = word.getMeasuredWidth();
                setUpClickiness(word);

                if (currentRemainingWidth - wordWidth <= 0) {
                    _layout.addView(hLayout);

                    hLayout = new LinearLayout(StageCoach.getContext());
                    hLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    hLayout.setOrientation(LinearLayout.HORIZONTAL);

                    currentRemainingWidth = TOTAL_WIDTH;
                }

                currentRemainingWidth -= wordWidth;
                hLayout.addView(word);

                if (pause != null) {

                    pause.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    int pauseWidth = pause.getMeasuredWidth();

                    if (currentRemainingWidth - pauseWidth <= 0) {
                        _layout.addView(hLayout);

                        hLayout = new LinearLayout(StageCoach.getContext());
                        hLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        hLayout.setOrientation(LinearLayout.HORIZONTAL);

                        currentRemainingWidth = TOTAL_WIDTH;
                    }

                    currentRemainingWidth -= pauseWidth;
                    hLayout.addView(pause);
                }
            }

            _layout.addView(hLayout);
            Toast.makeText(getActivity().getApplicationContext(), "The darker a word is, the louder it was", Toast.LENGTH_SHORT).show();
        }
        else
        {
            TextView error = new TextView(_layout.getContext());
            error.setText("Transcript processing is turned off, please turn it on to view transcript.");
            _layout.addView(error);
        }

        return _rootView;
    }

    private TranscriptWordView addPause() {
        TranscriptWordView pause = new TranscriptWordView(_layout.getContext(), "[PAUSE]", null);
        _words.add(pause);
        pause.display();
        _afterWordToTime.remove(0);
        return pause;
    }

    private Map<Integer, Double> getVolumesPerWord()
    {
        HashMap<Integer, Double> volumeAtWordNum = new HashMap<>();
        Map<Integer, Pair<Integer, Integer>> segments = sttm.getSegments();
        List words = sttm.getWords();

        for (int i = 0; i < words.size(); i++) {
            _FeedbackToWordID.add(new Pair(new ArrayList<Feedback>(), i));
        }

        PluginManager plugins = PluginManager.getInstance();

        Plugin vp = plugins.getPlugin("volume");
        Map<TimeRange, List<Feedback>> feedbackMap = vp.getFeedback();

        PausesPlugin pp = (PausesPlugin) plugins.getPlugin("pauses");

        //my lord this cast is unseemly
        List<Pair<List<Pair<Number, Number>>, Boolean>> graphData =
                (List<Pair<List<Pair<Number, Number>>, Boolean>>) pp.getData();



        boolean readingPause = false;
        List<Float> pauseMs = new ArrayList<>();
        for (Pair<List<Pair<Number, Number>>, Boolean> data: graphData) {
            Boolean tempBool = new Boolean(data.second);
            Pair<Number, Number> tempPair = new Pair(data.first.get(0).first, data.first.get(0).second);
            if (tempBool) {
                if (!readingPause) {
                    float timeInMs = tempPair.first.floatValue() * 1000;
                    pauseMs.add(timeInMs);
                    readingPause = true;
                }
            }
            else
                readingPause = false;
        }

        for (int i = 0; i < words.size(); i++) {
                int timeMS = segments.get(i).first;

                if (pauseMs.size() > 0) {
                    float pauseTime = pauseMs.get(0);
                    if (timeMS > pauseTime) {
                        pauseMs.remove(0);
                        Integer rounded = Math.round(pauseTime);
                        _afterWordToTime.add(new Pair(i - 1, rounded));
                    }
                }

                int roundedTimestamp = 250 * Math.round(timeMS / 250);

                for (TimeRange tr : feedbackMap.keySet()) {
                    if (tr.getStart() == roundedTimestamp) {
                        for (Feedback fb : feedbackMap.get(tr))
                            if (fb.getClass() == VolumeFeedback.class) {
                                double volumeFb = ((VolumeFeedback) fb).getVolume();
                                volumeAtWordNum.put(i, volumeFb);
                                if (fb != null)
                                    _FeedbackToWordID.get(i).first.add(fb);
                            }
                    }
                }
        }



        return volumeAtWordNum;
    }

    private void addFeedbacks() {
        Map<TimeRange, List<Feedback>> feedbackMap = PluginManager.getInstance().getPlugin("wordOveruse").getFeedback();
        for (int i = 0; i < _transcriptResults.size(); i++) {
            for (TimeRange tr : feedbackMap.keySet()) {
                for (Feedback fb : feedbackMap.get(tr)) {
                    if (fb.getClass() == WordOveruseFeedback.class) {
                        WordOveruseFeedback wfb = (WordOveruseFeedback) fb;
                        if (wfb.getWordCount().first.equals(_transcriptResults.get(i)))
                            _FeedbackToWordID.get(i).first.add(fb);
                    }
                }
            }
        }

        feedbackMap = PluginManager.getInstance().getPlugin("separators").getFeedback();
        for (int i = 0; i < _transcriptResults.size(); i++) {
            for (TimeRange tr : feedbackMap.keySet()) {
                for (Feedback fb : feedbackMap.get(tr)) {
                    if (fb.getClass() == SeparatorFeedback.class) {
                        SeparatorFeedback sf = (SeparatorFeedback) fb;
                            _FeedbackToWordID.get(sf.getWordNum()).first.add(fb);
                    }
                }
            }
        }
    }

    private void setUpClickiness(View v) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int viewID = _words.indexOf(view);

                AlertDialog.Builder popup = new AlertDialog.Builder(_layout.getContext());
                TextView forBox = new TextView(_layout.getContext());
                forBox.setText(((TranscriptWordView)view).getPopUpText());
                forBox.setTextSize(20);
                popup.setView(forBox);
                popup.show();
            }
        });
    }

    private String truncateWord(String str) {
        String result = "";
        if (str.contains("(")) {
            int startParen = str.indexOf('(');
            result = str.substring(0, startParen);
        }
        return result;
    }

}


