package test.stagecoach.UI;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import test.stagecoach.R;

/**
 * Created by chris_000 on 3/8/2016.
 */
public class SCPreferenceFragment extends PreferenceFragment {
    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
