package test.stagecoach.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.R;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;
import test.stagecoach.Utils.XMLifier;

public class QuickFeedbackActivity extends AppCompatActivity {

    TextView m_quickFeedback;
    String m_feedbackString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_feedback);

        TextView m_quickFeedback = (TextView)findViewById(R.id.quickFeedback);

        m_feedbackString = "";

        int seconds = SCAudioManager.getInstance().getDurationMS()/1000;
        int minutes = 0;
        if(seconds > 60) {
            minutes = seconds / 60;
            seconds = seconds % 60;
        }

        String html;
        if(minutes == 0)
            html = "&#8226;" + "Your speech was " + seconds + " seconds long.<br/><br/>";
        else if(minutes == 1)
            html = "&#8226;" + "Your speech was " + minutes + " minute and " +seconds + " seconds long.<br/><br/>";
        else
            html = "&#8226;" + "Your speech was " + minutes + " minutes and " +seconds + " seconds long.<br/><br/>";

        m_feedbackString += Html.fromHtml(html);

        for(Plugin p : PluginManager.getInstance().getPlugins()) {
            html = "&#8226;";
            html += p.getQuickFeedback();
            html += "<br/><br/>";
            m_feedbackString += Html.fromHtml(html);
        }

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        int dens=dm.densityDpi;
        double wi=(double)width/(double)dens;
        double hi=(double)height/(double)dens;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        double screenInches = Math.sqrt(x+y);
        
        if (screenInches > 6)
            m_quickFeedback.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        m_quickFeedback.setText(m_feedbackString);
    }

    public void onAttemptAgain(View v)
    {
        SCFileManager.getInstance().createAttemptDir();
        SCAudioManager.reset();
        PluginManager.getInstance().reset();
        STTManager.getInstance().reset();
        XMLifier.getInstance().reset();
        Intent i = new Intent(this, SpeechActivity.class);
        this.startActivity(i);
    }

    public void onBreakdown(View v)
    {
        Intent intent = new Intent(getBaseContext(), ResultsActivity.class);
        startActivity(intent);
    }
}
