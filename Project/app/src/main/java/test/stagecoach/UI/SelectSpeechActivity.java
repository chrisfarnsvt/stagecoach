package test.stagecoach.UI;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.menu.ListMenuItemView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.io.File;
import java.util.ArrayList;

import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.R;
import test.stagecoach.StageCoach;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;

public class SelectSpeechActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView speechList;
    private ArrayList<String> speechNames = new ArrayList<String>();
    private SCFileManager m_fm;
    private ArrayAdapter<String> speechesAdapter;
    private SCAudioManager audioManager;
    private PluginManager pluginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_speech);

        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        speechList = (ListView)findViewById(R.id.speechList);
        speechList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        STTManager.initialize(StageCoach.getContext());
        m_fm = SCFileManager.getInstance(getApplicationContext().getDir("Speeches", Context.MODE_PRIVATE));
        audioManager = SCAudioManager.getInstance();
        pluginManager = PluginManager.getInstance();

        for(File f : m_fm.getSpeechDirs())
        {
            if(f.isDirectory())
            {
                speechNames.add(f.getName());
            }
        }


        speechesAdapter = new ArrayAdapter<String>(SelectSpeechActivity.this, android.R.layout.simple_list_item_1, speechNames);

        speechList.setAdapter(speechesAdapter);
        speechList.setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_speech, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SCPreferenceActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String dir = (String)parent.getAdapter().getItem(position);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        Point p = new Point();
        p.set(location[0], location[1]);

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout)findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);
        speechesAdapter.insert("", position+1);
        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(SelectSpeechActivity.this);
        popup.setContentView(layout);
        popup.setWindowLayoutMode(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        popup.setHeight(1);
        popup.setWidth(1);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        //int OFFSET_X = view.getWidth()/2;
        int OFFSET_Y = view.getHeight();

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x, p.y + OFFSET_Y);

        //setup on click listeners for popup buttons
        m_fm.setSpeechDir(dir);
        PopupOnClickListener ocl = new PopupOnClickListener(SelectSpeechActivity.this, speechList, speechNames, popup);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                speechesAdapter.remove("");
            }
        });
        layout.findViewById(R.id.Attempt).setOnClickListener(ocl);
        layout.findViewById(R.id.Review).setOnClickListener(ocl);
        layout.findViewById(R.id.Delete).setOnClickListener(ocl);
    }

    public void onNewSpeechClicked(View v)
    {
        //gather data for new speech
        Dialog getName = new Dialog(SelectSpeechActivity.this);

        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        Window window = this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

// inflate and adjust layout
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getName.getWindow().getAttributes());
        lp.width = (int) ( displayRectangle.width() * 0.75);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getName.show();
        getName.getWindow().setAttributes(lp);

        getName.setTitle("Enter a Name");

        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText t = new EditText(SelectSpeechActivity.this);
        Button done = new Button(SelectSpeechActivity.this);
        done.setText("Done");
        done.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(), SpeechActivity.class);
                    String filename = t.getText().toString();
                    if(filename.contains(" "))
                        filename = filename.replaceAll("\\s+","");
                    m_fm.createSpeechDir(filename);
                    m_fm.createAttemptDir();
                    startActivity(i);
                }
        });
        layout.addView(t);
        layout.addView(done);
        getName.addContentView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));

        getName.show();
        //start main activity
    }


}
