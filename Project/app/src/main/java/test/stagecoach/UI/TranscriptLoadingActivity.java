package test.stagecoach.UI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.STTManager;
import test.stagecoach.Utils.XMLifier;

/**
 * Created by chris_000 on 1/29/2016.
 */
public class TranscriptLoadingActivity extends Activity {


        //A ProgressDialog object
        private ProgressDialog progressDialog;

        /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            //Initialize a LoadViewTask object and call the execute() method
            new LoadViewTask().execute();

        }

        //To use the AsyncTask, it must be subclassed
        private class LoadViewTask extends AsyncTask<Void, Integer, Void>
        {
            //Before running code in separate thread
            @Override
            protected void onPreExecute()
            {
                //Create a new progress dialog
                progressDialog = ProgressDialog.show(TranscriptLoadingActivity.this,"Processing...",
                        "Processing transcript, this takes a while...", false, false);
            }

            //The code to be executed in a background thread.
            @Override
            protected Void doInBackground(Void... params)
            {
            /* This is just a code that delays the thread execution 4 times,
             * during 850 milliseconds and updates the current progress. This
             * is where the code that is going to be executed on a background
             * thread must be placed.
             */
                try
                {
                    //Get the current thread's token
                    synchronized (this)
                    {
                        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                        boolean makeTranscript = SP.getBoolean("transcriptGen", true);
                        if (makeTranscript) {
                            STTManager sttm;
                            if (!SCFileManager.doesXMLExist()) {
                                //Initialize an integer (that will act as a counter) to zero
                                sttm = STTManager.getInstance();

                                sttm.translate(SCAudioManager.getInstance().getRecordingLocation(),
                                        SCAudioManager.getInstance().getWavFileName());

                                PluginManager.getInstance().loadAllPlugins();
                                PluginManager.getInstance().process();

                                XMLifier xmlGen = XMLifier.getInstance();
                                xmlGen.buildOutputString();
                                Log.d("DEBUG", xmlGen.getDocumentText());
                                SCFileManager.writeSpeechXMLData(xmlGen.getDocumentText());
                            } else {
                                sttm = STTManager.getInstance();
                                sttm.readXMLTag();

                                PluginManager.getInstance().loadAllPlugins();

                                for(Plugin p : PluginManager.getInstance().getPlugins())
                                {
                                    p.readXMLTag();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return null;
            }

            //Update the progress
            @Override
            protected void onProgressUpdate(Integer... values)
            {
                //set the current progress of the progress dialog
                progressDialog.setProgress(values[0]);
            }

            //after executing the code in the thread
            @Override
            protected void onPostExecute(Void result)
            {
                //close the progress dialog
                progressDialog.dismiss();
                //initialize the View
                Intent intent = new Intent(getBaseContext(), QuickFeedbackActivity.class);
                startActivity(intent);
            }
        }
}

