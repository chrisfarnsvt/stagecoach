package test.stagecoach.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.androidplot.Plot;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.XLayoutStyle;
import com.androidplot.ui.YLayoutStyle;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.R;
import test.stagecoach.Utils.SCFileManager;
import test.stagecoach.Utils.SCPlotManager;

public class TrendsActivity extends AppCompatActivity implements View.OnClickListener {

    XYPlot m_trendPlot;
    PluginManager m_pluginManager;
    Map<String, List<Integer>> m_pluginScores;
    private int m_chosen;
    String m_selected = "average";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trends);

        m_pluginManager = PluginManager.getInstance();
        m_trendPlot = (XYPlot) findViewById(R.id.trendPlot);
        m_pluginScores = new HashMap();


        for (File f : SCFileManager.getInstance().getAttempts()) {
            File inputFile = new File(f.getPath() + "/speechData.xml");
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = null;
            try {
                dBuilder = dbFactory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            Document doc = null;
            try {
                doc = dBuilder.parse(inputFile);
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            doc.getDocumentElement().normalize();
            Node avgScoreElem = doc.getElementsByTagName("avgScore").item(0);
            NodeList list = doc.getElementsByTagName("score");
            for (int i = 0; i < list.getLength(); i++) {
                Node scoreNode = list.item(i);
                Node parent = scoreNode.getParentNode();
                String name = parent.getNodeName();
                int score = Integer.parseInt(scoreNode.getTextContent());
                if (m_pluginScores.get(name) == null) {
                    m_pluginScores.put(name, new ArrayList<Integer>());
                }
                m_pluginScores.get(name).add(score);
            }


            if (m_pluginScores.get("average") == null) {
                m_pluginScores.put("average", new ArrayList<Integer>());
            }

            m_pluginScores.get("average").add(Integer.parseInt(avgScoreElem.getTextContent()));
        }

        initialize();

        m_trendPlot.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        final CharSequence[] pluginNames = new CharSequence[m_pluginScores.size()];

        int pos = 0;
        int selected = 0;
        for (Map.Entry<String, List<Integer>> entry : m_pluginScores.entrySet()) {
            pluginNames[pos] = entry.getKey();
            if (entry.getKey() == m_selected)
                selected = pos;
            pos++;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Set the dialog title
        builder.setTitle("Feedback Categories")
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected

                .setSingleChoiceItems(pluginNames, selected, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        m_chosen = which;
                        m_selected = pluginNames[m_chosen].toString();
                    }
                })
                        // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, so save the mSelectedItems results somewhere
                        // or return them to the component that opened the dialog
                        initialize();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).create().show();
        //selected plugin changes series
    }

    private void initialize()
    {
        XYGraphWidget graphWidget = m_trendPlot.getGraphWidget();

        SizeMetrics sm = new SizeMetrics(30, SizeLayoutType.FILL, 0, SizeLayoutType.FILL);
        graphWidget.setSize(sm);
        graphWidget.position(0, XLayoutStyle.ABSOLUTE_FROM_LEFT, 30, YLayoutStyle.ABSOLUTE_FROM_TOP);

        m_trendPlot.clear();
        m_trendPlot.invalidate();

        m_trendPlot.setRangeBoundaries(0, 100, BoundaryMode.FIXED);
        m_trendPlot.setRangeStepValue(10);
        m_trendPlot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 10);
        m_trendPlot.setRangeValueFormat(new DecimalFormat("0"));
        m_trendPlot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
        m_trendPlot.setBackgroundColor(Color.WHITE);
        m_trendPlot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
        m_trendPlot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);

        m_trendPlot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
        m_trendPlot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

        m_trendPlot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
        m_trendPlot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        m_trendPlot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);

        m_trendPlot.getLayoutManager().remove(m_trendPlot.getLegendWidget());
        m_trendPlot.setPlotMargins(0, 0, 0, 0);
        Paint title = m_trendPlot.getTitleWidget().getLabelPaint();

        ArrayList<Number> xVals = new ArrayList<>();
        ArrayList<Number> yVals = new ArrayList<>();

        //xVals.add(0);
        //yVals.add(0);

        int pos = 1;
        for(Integer i : m_pluginScores.get(m_selected))
        {
            xVals.add(pos);
            yVals.add(i);
            pos++;
        }

        XYSeries avgTrend = new SimpleXYSeries(xVals, yVals, m_selected);


        int domain = xVals.size();
        if (domain < 5)
            domain = 5;

        m_trendPlot.setDomainBoundaries(0, domain, BoundaryMode.FIXED);
        m_trendPlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
        LineAndPointFormatter seriesFormat = new LineAndPointFormatter(Color.rgb(0, 255, 0),
                null,
                null,
                null);

        Paint lp = seriesFormat.getLinePaint();
        lp.setStrokeWidth(5);
        seriesFormat.setLinePaint(lp);

        m_trendPlot.addSeries(avgTrend, seriesFormat);
    }
}
