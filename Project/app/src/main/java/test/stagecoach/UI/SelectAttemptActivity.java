package test.stagecoach.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

import test.stagecoach.R;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;

public class SelectAttemptActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView attemptList;
    private ArrayList<String> attemptNames = new ArrayList<String>();
    private SCFileManager m_fm;
    private ArrayAdapter<String> attemptAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_attempt);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        attemptList = (ListView)findViewById(R.id.attemptList);
        attemptList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        m_fm = SCFileManager.getInstance();

        for(File f : m_fm.getAttempts())
        {
            if(f.isDirectory())
            {
                attemptNames.add(f.getName());
            }
        }


        attemptAdapter = new ArrayAdapter<String>(SelectAttemptActivity.this, android.R.layout.simple_list_item_1, attemptNames);

        attemptList.setAdapter(attemptAdapter);
        attemptList.setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_speech, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //gather data for new speech

        Intent i = new Intent(getApplicationContext(), TranscriptLoadingActivity.class);
        String dir = (String)parent.getAdapter().getItem(position);
        m_fm.setAttemptDir(dir); // make sure we are looking at the right attempt
        SCAudioManager.getInstance().updateFileLocation();
        startActivity(i);
    }

    public void onTrendsClicked(View v)
    {
        Intent i = new Intent(getApplicationContext(), TrendsActivity.class);
        startActivity(i);
    }


}
