package test.stagecoach.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.xy.XYPlot;

import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.R;
import test.stagecoach.Utils.SCPlotManager;

/**
 * Created by colin on 2/11/2016.
 */
public class GraphFragment extends Fragment {

    public static final String ARG_OBJECT = "object";

    private SCPlotManager m_plotManager;
    private PluginManager m_pm;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
        View rootView = inflater.inflate(
                R.layout.fragment_graph, container, false);

        m_pm = PluginManager.getInstance();
        SCPlotManager.initialize((XYPlot) rootView.findViewById(R.id.mySimpleXYPlot), this);

        m_plotManager = SCPlotManager.getInstance();
        m_plotManager.initializeGraph(m_pm.getPlugin("volume")); //start off with the volume plugin

        return rootView;
    }
}
