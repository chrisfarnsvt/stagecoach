package test.stagecoach.UI;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by colin on 2/9/2016.
 */
// Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
public class SCPagerAdapter extends FragmentStatePagerAdapter {
    public SCPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch(i)
        {
            case 0:
                 return new SummaryFragment();
            case 1:
                return new GraphFragment();
            case 2:
                return new TranscriptFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Summary";
            case 1:
                return "Graph";
            case 2:
                return "Transcript";
        }
        return ""; // wtf how did you get here?
    }
}
