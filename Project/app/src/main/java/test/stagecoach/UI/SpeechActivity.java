package test.stagecoach.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import test.stagecoach.R;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SCFileManager;

public class SpeechActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView status;
    private SCAudioManager m_audioManager;
    private SCFileManager m_fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.speech_layout);
        rl.setOnClickListener(this);

        m_audioManager = SCAudioManager.getInstance();
        m_fileManager = SCFileManager.getInstance();

        status = (TextView) findViewById(R.id.status);

        setStatus("Tap to begin recording");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(m_audioManager.isRecording())
            endSpeechInput();
        else
            promptSpeechInput();
    }

    public void promptSpeechInput()
    {
        setStatus("Recording, Tap to Finish");
        m_audioManager.startRecording();
    }

    public void endSpeechInput()
    {
        setStatus("Recording Done!");
        m_audioManager.stopRecording();

        Intent i = new Intent(getApplicationContext(), TranscriptLoadingActivity.class);
        startActivity(i);
    }

    //if you leave str blank, the default setting is "Waiting for you..."
    private void setStatus(String str) {
        if (str.equals(""))
            status.setText("Waiting for you...");
        else
            status.setText(str);
    }
}
