package test.stagecoach.UI;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import test.stagecoach.Analysis.Plugin;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.R;
import test.stagecoach.StageCoach;
import test.stagecoach.Utils.AudioRunnable;
import test.stagecoach.Utils.SCAudioManager;
import test.stagecoach.Utils.SeekBarPositioner;

/**
 * Created by colin on 2/9/2016.
*/

// Instances of this class are fragments representing a single
// object in our collection.
public class SummaryFragment extends Fragment implements Button.OnClickListener{
    public static final String ARG_OBJECT = "object";

    private SCAudioManager m_audioManager;
    private PluginManager m_pluginManager;
    private SeekBar m_seekBar;
    private View m_rootView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
        m_rootView = inflater.inflate(
                R.layout.fragment_summary, container, false);

        m_audioManager = SCAudioManager.getInstance();
        m_seekBar = (SeekBar)m_rootView.findViewById(R.id.seekBar);

        m_rootView.findViewById(R.id.playButton).setOnClickListener(this);
        m_rootView.findViewById(R.id.pauseButton).setOnClickListener(this);

        m_pluginManager = PluginManager.getInstance();

        int totalSeconds = m_audioManager.getDurationMS()/1000;
        int minutes = totalSeconds / 60;
        int seconds = totalSeconds % 60;
        String dur;
        if(seconds < 10)
            dur = Integer.toString(minutes) + ":0" + Integer.toString(seconds);
        else
            dur = Integer.toString(minutes) + ":" + Integer.toString(seconds);

        ((TextView)m_rootView.findViewById(R.id.duration)).setText(dur);
        postCreate();

        return m_rootView;
    }

    private SeekBarPositioner sbp;
    private Thread position;
    private AudioRunnable ar;
    private Thread audio;

    public void onClick(View v)
    {
       if(v.getId() == R.id.playButton)
           onPlay();
        else if(v.getId() == R.id.pauseButton)
           onPauseButtonClicked();
    }


    public void onPlay()
    {
        if(position != null && audio != null && ar.isPaused() && sbp.isPaused())
        {
            ar.resume();
            sbp.resume();
        }
        else
        {
            ar = new AudioRunnable(m_audioManager);
            sbp = new SeekBarPositioner(m_seekBar, (TextView)m_rootView.findViewById(R.id.position), (TextView)m_rootView.findViewById(R.id.duration));

            position = new Thread(sbp);
            position.start();
            audio = new Thread(ar);
            audio.start();
        }
    }

    public void onPauseButtonClicked()
    {
        m_audioManager.pausePlayback();
        ar.pause();
        sbp.pause();
    }

    public void postCreate()
    {
        LinearLayout scoresLayout = (LinearLayout)m_rootView.findViewById(R.id.scoresLayout);
        scoresLayout.setBackgroundResource(R.drawable.score_background);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int TOTAL_WIDTH = metrics.widthPixels - 10;
        int currentRemainingWidth = TOTAL_WIDTH;
        int test = scoresLayout.getWidth();

        for(Plugin p : m_pluginManager.getPlugins()) {
            LinearLayout stack = new LinearLayout((StageCoach.getContext()));
            stack.setOrientation(LinearLayout.VERTICAL);
            LinearLayout row = new LinearLayout(StageCoach.getContext());
            row.setOrientation(LinearLayout.HORIZONTAL);
            row.setPadding(40, 0, 0, 0);

            TextView name = new TextView(StageCoach.getContext());
            name.setText(p.getID());
            name.setTextColor(Color.BLACK);
            name.setTextSize(15);
            name.setPadding(10, 0, 0, 0);
            name.setWidth((int) ((TOTAL_WIDTH * 0.2)));
            stack.addView(name);
            //name.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

            //ScoreView scoreVis = new ScoreView(StageCoach.getContext(), null, p.getPercentScore());
            TextView color = new TextView(StageCoach.getContext());
            color.setBackgroundColor(Color.GREEN);
            double scoreRatio = (double)p.getPercentScore()/100;
            int colorWidth = (int)((TOTAL_WIDTH*0.4)*scoreRatio);
            color.setWidth(colorWidth);

            TextView color2 = new TextView(StageCoach.getContext());
            color2.setBackgroundColor(Color.RED);
            color2.setWidth((int) (TOTAL_WIDTH * 0.4) - colorWidth);

            TextView score = new TextView(StageCoach.getContext());
            score.setText(Integer.toString(p.getPercentScore()) + " / 100");
            score.setTextColor(Color.BLACK);
            score.setTextSize(15);
            score.setPadding(10, 0, 10, 0);
            score.setWidth((int) ((TOTAL_WIDTH * 0.4)));

            row.addView(color);
            row.addView(color2);
            row.addView(score);

            stack.addView(row);

            View line = new View(StageCoach.getContext());
            LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
            lineParams.setMargins(0, 10, 0, 4);
            line.setBackgroundColor(Color.BLACK);
            line.setLayoutParams(lineParams);
            stack.addView(line);

            scoresLayout.addView(stack);

            scoresLayout.setBackgroundColor(Color.WHITE);
        }
    }

}
