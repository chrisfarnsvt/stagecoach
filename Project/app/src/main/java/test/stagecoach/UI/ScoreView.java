package test.stagecoach.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by colin on 2/18/2016.
 */
public class ScoreView extends View {

    private int m_score;

    private int width;
    private int height;

    public ScoreView(Context context, AttributeSet attrs, int score)
    {
        super(context, attrs);
        m_score = score;
    }

    @Override
    protected void onDraw (Canvas canvas)
    {
        Paint p = new Paint();
        p.setColor(Color.BLACK);
        //canvas.drawRect(0, 0, width, height, p);
        canvas.drawColor(Color.BLACK);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        width = w;
        height = h;
        invalidate();
    }

}
