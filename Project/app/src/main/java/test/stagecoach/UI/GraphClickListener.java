package test.stagecoach.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Utils.GraphType;
import test.stagecoach.Utils.SCPlotManager;

/**
 * Created by colin on 1/10/2016.
 */
public class GraphClickListener implements View.OnClickListener {

    private PluginManager m_pluginManager;
    private int m_SelectedItem;
    private GraphFragment m_parent;

    public GraphClickListener(GraphFragment parent)
    {
        m_pluginManager = PluginManager.getInstance();
        m_parent = parent;
    }
    @Override
    public void onClick(View view) {

        final CharSequence[] pluginNames = new CharSequence[m_pluginManager.getPlugins().size()];

        int curSelected = -1;
        for(int i = 0; i < m_pluginManager.getPlugins().size(); i++)
        {
            pluginNames[i] = m_pluginManager.getPlugins().get(i).getID();
            if(SCPlotManager.getInstance().getCurPluginID().equals(pluginNames[i]))
            {
                curSelected = i;
            }
        }

        int numGraphPlugins = 0;
        for(CharSequence cs : pluginNames)
        {
            if(m_pluginManager.getPlugin(cs.toString()).getType() == GraphType.xy_scatter)
            {
                numGraphPlugins++;
            }
        }

        final CharSequence[] graphPluginNames = new CharSequence[numGraphPlugins];

        int pos = 0;
        for(CharSequence cs : pluginNames)
        {
            if(m_pluginManager.getPlugin(cs.toString()).getType() == GraphType.xy_scatter)
            {
                graphPluginNames[pos] = cs;
                if(SCPlotManager.getInstance().getCurPluginID().equals(graphPluginNames[pos]))
                        curSelected = pos;

                pos++;
            }
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(m_parent.getActivity());
        // Set the dialog title
        builder.setTitle("Feedback Categories")
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected

                .setSingleChoiceItems(graphPluginNames, curSelected, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
               // The 'which' argument contains the index position
               // of the selected item
                   m_SelectedItem = which;
            }})
                        // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, so save the mSelectedItems results somewhere
                        // or return them to the component that opened the dialog
                        SCPlotManager.getInstance().initializeGraph(m_pluginManager.getPlugin((String) pluginNames[m_SelectedItem]));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).create().show();
    }
}
