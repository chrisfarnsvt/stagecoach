package test.stagecoach.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import test.stagecoach.R;
import test.stagecoach.Utils.SCFileManager;

/**
 * Created by colin on 11/15/2015.
 */
public class PopupOnClickListener implements View.OnClickListener {

    private Context m_context;
    private ListView m_speechList;
    private ArrayList<String> m_speechNames;
    private PopupWindow m_popup;
    private SCFileManager m_fm;

    PopupOnClickListener(Context context, ListView speechList, ArrayList<String> speechNames, PopupWindow popup)
    {
        //Assert(speechDir.isDirectory());
        m_context = context;
        m_speechList = speechList;
        m_speechNames = speechNames;
        m_popup = popup;
        m_fm = SCFileManager.getInstance();
    }

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.Review:
                //review the speeches past results
                Intent at = new Intent(m_context, SelectAttemptActivity.class);
                m_context.startActivity(at);
                break;
            case R.id.Attempt:
                //attempt the speech again
                m_fm.createAttemptDir();
                Intent i = new Intent(m_context, SpeechActivity.class);
                m_context.startActivity(i);
                break;
            case R.id.Delete:
                //delete the speech

                //confirm user input
               AlertDialog d =  new AlertDialog.Builder(m_context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete " + m_fm.getSpeechName())
                        .setMessage("Are you sure you want to delete the speeech \"" + m_fm.getSpeechName() + "\" and all its attempts?")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                    deleteSpeech();
                            }

                        })
                        .setNegativeButton("Cancel", null)
                        .show();
                m_popup.dismiss();
                break;
        }
    }


    private void deleteSpeech()
    {
        m_fm.deleteCurrentSpeech();
        m_speechNames.remove(m_speechList.getCheckedItemPosition());
        ArrayAdapter<String> aa = (ArrayAdapter<String>) m_speechList.getAdapter();
        aa.notifyDataSetChanged();
    }

}
