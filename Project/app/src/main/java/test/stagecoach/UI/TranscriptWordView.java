package test.stagecoach.UI;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import test.stagecoach.Analysis.Feedback;
import test.stagecoach.Analysis.PluginManager;
import test.stagecoach.Analysis.Plugins.SeparatorFeedback;
import test.stagecoach.Analysis.Plugins.VolumeFeedback;
import test.stagecoach.Analysis.Plugins.WordOveruseFeedback;

/**
 * Created by chris_000 on 3/11/2016.
 */
public class TranscriptWordView extends TextView {
    private List<Feedback> _feedbacks;
    private List<Feedback> _errors;
    private String _word;
    private String _popUpText = "";
    private boolean volumeDone = false;

    public TranscriptWordView(Context context, String word, List<Feedback> feedbacks) {
        super(context);
        _word = word;
        _feedbacks = feedbacks;
        _errors = new ArrayList<>();
    }

    public void display() {
        removeGarbage();
        setText(_word + " ");
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        if (_word.equals("[PAUSE]")) {
            setText("...");
            setTextColor(Color.BLUE);
            setBackgroundColor(Color.YELLOW);
        }
        else {
            checkErrors();
            buildPopupString();

            for (Feedback feedback : _feedbacks) {
                if (feedback.getClass() == VolumeFeedback.class) {
                    VolumeFeedback volFeed = (VolumeFeedback) feedback;
                    double volume = volFeed.getVolume();
                    if (volume > 60)
                        setTextColor(Color.parseColor("#000000"));
                    else if (volume > 55)
                        setTextColor(Color.parseColor("#444444"));
                    else if (volume > 50)
                        setTextColor(Color.parseColor("#888888"));
                    else
                        setTextColor(Color.parseColor("#bbbbbb"));
                }

                if (feedback.getClass() == SeparatorFeedback.class) {
                    setTextColor(Color.parseColor("#FFFF00"));
                }
            }

            setErrorStyling();
        }
    }

    public List<Feedback> getFeedbacks() {
        return _feedbacks;
    }

    public List<Feedback> getErrors() {
        return _errors;
    }

    public String getPopUpText() {
        return _popUpText;
    }

    private void checkErrors() {
        for (Feedback feed : _feedbacks) {
            if (feed.getClass() == VolumeFeedback.class) {
                VolumeFeedback volFeed = (VolumeFeedback) feed;
                if( volFeed.getVolume() < 60)
                    _errors.add(feed);
            }

            if (feed.getClass() == WordOveruseFeedback.class) {
                WordOveruseFeedback wFeed = (WordOveruseFeedback) feed;
                if (wFeed.getWordCount().second > 3) {
                    _errors.add(feed);
                }
            }
        }
    }

    private void buildPopupString() {
        for (Feedback feed : _feedbacks) {
            if (feed.getClass() == VolumeFeedback.class && !volumeDone) {
                String addendum = "";
                VolumeFeedback volFeed = (VolumeFeedback) feed;
                double volume = volFeed.getVolume();

                if(volume <= 75 && volume >= 65)
                    addendum = "Your volume on this word was perfect!";

                if(volume > 75)
                    addendum = "You spoke this word very loudly, try lowering your voice";

                if(volume  < 65 && volume > 50)
                    addendum = "You spoke this word a little quietly, try raising your voice slightly";

                if(volume  <= 50)
                    addendum = "You spoke this word very quietly, try raising your voice";

                _popUpText += addendum + "\n";
                volumeDone = true;
            }

            if (feed.getClass() == WordOveruseFeedback.class) {
                WordOveruseFeedback wFeed = (WordOveruseFeedback) feed;
                _popUpText += "Number of times word appears: " + wFeed.getWordCount().second + "\n";
            }
        }
    }

    private void setErrorStyling() {
        if(_errors.size() > 0) {
            setBackgroundColor(Color.parseColor("#FBA7BC"));
            for (Feedback feedback : _errors) {

            }
        }
    }

    private void removeGarbage() {
        if (_word.contains("("))
            _word = _word.substring(0, _word.indexOf("("));
    }
}
